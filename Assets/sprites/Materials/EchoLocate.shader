﻿Shader "Custom/Echolocate" {

    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)

        _EchoColor("EchoColor", Color) = (1,1,1,1)
        _Center("Center", vector) = (0,0,0)
        _Radius("Radius",Float) = 1
        _Thickness("Thickness",Float) = .5

        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf PointLambert vertex:vert nofog nolightmap nodynlightmap keepalpha noinstancing
        #pragma multi_compile _ PIXELSNAP_ON
        #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
        #include "UnitySprites.cginc"


        fixed4 _EchoColor;
        float3 _Center;
        float _Radius;
        float _Thickness;

        half4 LightingPointLambert(SurfaceOutput s, half3 lightDir, half atten) {
        	half4 c;
        	//c.rgb = s.Emission * s.Albedo;
        	c.rgb = s.Specular * _EchoColor * s.Albedo;
            c.rgb += s.Albedo * _LightColor0.rgb * atten;
            c.a = s.Alpha;
        	return c;
        }

        struct Input
        {
            float2 uv_MainTex;
            fixed4 color;
            float3 worldPos;
        };

        void vert (inout appdata_full v, out Input o)
        {
        	v.vertex = UnityFlipSprite(v.vertex, _Flip);

        	//float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

            
            #if defined(PIXELSNAP_ON)
            v.vertex = UnityPixelSnap (v.vertex);
            #endif	

            UNITY_INITIALIZE_OUTPUT(Input, o);
            o.color = v.color * _Color * _RendererColor;
        }

        void surf (Input IN, inout SurfaceOutput o)
        {
        	float dist = distance(_Center, IN.worldPos);
            half echo = smoothstep(_Radius - _Thickness, _Radius, dist) - smoothstep(_Radius, _Radius + _Thickness, dist );

            fixed4 c = SampleSpriteTexture (IN.uv_MainTex) * IN.color;
            o.Albedo = c.rgb * c.a;
            o.Alpha = c.a;
            //o.Emission = _EchoColor * echo;
            o.Specular = echo;
        }
        ENDCG
    }

Fallback "Diffuse"
}
