﻿Shader "Custom/DynObjMark" {
	
	Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)

        _PlayerPosition("Player Position", Vector) = (0,0,0,0)
		_FadeRadiusStart("Fade Radius Start", float) = 1
		_FadeRadiusFinish("Fade Radius Finish", float) = 1
     
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf PointLambert vertex:vert nofog nolightmap nodynlightmap keepalpha noinstancing
        #pragma multi_compile _ PIXELSNAP_ON
        #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
        #include "UnitySprites.cginc"


		fixed2 _PlayerPosition;
		fixed _FadeRadiusStart;
		fixed _FadeRadiusFinish;


        half4 LightingPointLambert(SurfaceOutput s, half3 lightDir, half atten) {
        	half4 c;
			c.rgb = s.Albedo * atten;
            c.a = s.Alpha;
        	return c;
        }

        struct Input
        {
            float2 uv_MainTex;
            fixed4 color;
            float3 worldPos;
        };

        void vert (inout appdata_full v, out Input o)
        {
        	v.vertex = UnityFlipSprite(v.vertex, _Flip);
        	
            #if defined(PIXELSNAP_ON)
            v.vertex = UnityPixelSnap (v.vertex);
            #endif	

            UNITY_INITIALIZE_OUTPUT(Input, o);
            o.color = v.color * _Color * _RendererColor;
        }

        void surf (Input IN, inout SurfaceOutput o)
        {
        	fixed4 c = SampleSpriteTexture (IN.uv_MainTex) * IN.color;
            
            fixed dist = distance(_PlayerPosition.xy, IN.worldPos.xy);
            fixed alphaMul = smoothstep(_FadeRadiusStart, _FadeRadiusFinish, dist);
            
			o.Albedo = c.rgb * c.a * alphaMul;
            o.Alpha = c.a * alphaMul;
			o.Gloss = alphaMul;
        }
        ENDCG
    }

Fallback "Diffuse"
}
