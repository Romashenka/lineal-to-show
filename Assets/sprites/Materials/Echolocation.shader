﻿Shader "Custom/Echolocation"
{
	Properties
	{
		//_MainTex ("Texture", 2D) = "white" {}
		_EchoColor("EchoColor", Color) = (1,1,1,1)
        _Center("Center", vector) = (0,0,0)
        _Radius("Radius",Float) = 1
        _Thickness("Thickness",Float) = .5


	}
	SubShader
	{
		Tags 
		{ 
		"Queue"="Transparent"
		"RenderType"="Transparent" 
		//"IgnoreProjector"="True"
		}


		Blend SrcAlpha OneMinusSrcAlpha
		//Blend OneMinusDstColor One
		//BlendOp Max, Max
		//Offset 0,-1
		//ZTest off
		ZWrite off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			//#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				//float3 worldPos : TEXCOORD0;
			};

			struct v2f
			{
				//float echo : TEXCOORD0;
				//UNITY_FOG_COORDS(1)
				float3 worldPos : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			//sampler2D _MainTex;
			//float4 _MainTex_ST;
			fixed4 _EchoColor;
        	float3 _Center;
        	float _Radius;
        	float _Thickness;



			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld,v.vertex);
				
				//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				//UNITY_TRANSFER_FOG(o,o.vertex);

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				float dist = distance(_Center.xy, i.worldPos.xy);
            	half echo = smoothstep(_Radius - _Thickness, _Radius, dist) - smoothstep(_Radius, _Radius + _Thickness, dist );

            	//fixed4 col;
				fixed4 col = _EchoColor * echo;
				return col;
			}
			ENDCG
		}
	}
}
