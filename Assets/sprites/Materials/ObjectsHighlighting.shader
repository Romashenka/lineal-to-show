﻿Shader "Custom/ObjectsHighlighting"
{
	Properties
	{
		//_MainTex ("Texture", 2D) = "white" {}

		_EchoColor ("HighlightColor",color) = (1,1,1,1)
		_ZCoord ("ZCoord",float) = 0
		_Blur ("Blur", float) = 1

		_Center("Center", vector) = (0,0,0)
        _Radius("Radius",Float) = 1
        _Thickness("Thickness",Float) = .5
	}
	SubShader
	{
		Tags { "RenderType"="Transperent" }
		//LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				half4 vertex : POSITION;
				//float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				//float2 uv : TEXCOORD0;
				//UNITY_FOG_COORDS(1)
				half4 vertex : SV_POSITION;
				half3 worldPos : TEXCOORD0;
			};

			//sampler2D _MainTex;
			//float4 _MainTex_ST;
			fixed4 _EchoColor;
			fixed _ZCoord;
			fixed _Blur;

			fixed3 _Center;
        	fixed _Radius;
        	fixed _Thickness;


			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				//UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float dist = distance(_Center.xy, i.worldPos.xy);
            	
            	fixed highLightXY = smoothstep(_Radius, _Radius - _Thickness, dist);
				fixed highLightZ = smoothstep(_ZCoord - _Blur, _ZCoord, i.worldPos.z);
				// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return _EchoColor * highLightZ * highLightXY * _EchoColor.a;
			}
			ENDCG
		}
	}
}
