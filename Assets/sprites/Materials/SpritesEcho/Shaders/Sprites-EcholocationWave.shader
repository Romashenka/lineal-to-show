﻿Shader "Sprites/Custom/Sprites-EcholocationWave" {

    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)

            _EchoColor("EchoColor", Color) = (1,1,1,1)
            _EchoAlpha1("EchoAlpha1", Range(0,1)) = 1
            _EchoAlpha2("EchoAlpha2", Range(0,1)) = 1
            _Center1("Center1", vector) = (0,0,0)
            _Center2("Center2", vector) = (0,0,0)
            _Radius1("Radius1",Float) = 1
            _Radius2("Radius2", float) = 1
            _Thickness("Thickness",Float) = .5

        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Overlay"
            "IgnoreProjector"="True"
            "RenderType"="Overlay"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf PointLambert finalcolor:final vertex:vert nofog nolightmap nodynlightmap keepalpha noinstancing 
        
        #pragma multi_compile _ PIXELSNAP_ON
        #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
        #include "UnitySprites.cginc"

        fixed4 _EchoColor;
        fixed _EchoAlpha1;
        fixed _EchoAlpha2;
        fixed3 _Center1;
        fixed3 _Center2;
        fixed _Radius1;
        fixed _Radius2;
        float _Thickness;

        
        struct Input
        {
            float2 uv_MainTex;
            fixed4 color;
            float3 worldPos;
        };

        struct MySurfaceOutput
        {
            fixed3 Albedo;
            fixed3 Normal;
            fixed3 Emission;
            half Specular;
            fixed Gloss;
            fixed Alpha;
            fixed echo1;
            fixed echo2;
            fixed atten;
        };

        void final(Input IN, MySurfaceOutput s,inout fixed4 color)
        {
            //echoed = saturate(echoed);
            //color.rgb += echoed;
        }

        half4 LightingPointLambert(MySurfaceOutput s, half3 lightDir, half atten) {
        	fixed4 c;
        	fixed echoA1 = _EchoAlpha1 * s.echo1;
            fixed echoA2 = _EchoAlpha2 * s.echo2;
            fixed echoAlpha = max(echoA1,echoA2);
            fixed3 echoed = _EchoColor * echoAlpha * _EchoColor.a * atten;
            //echoed *=1 -sign(-lighted.r)+sign(-lighted.g)+sign(-lighted.b)+sign(_LightColor0.a);
            //fixed signA = saturate(sign(color.b));
            
            fixed3 lighted = _LightColor0.rgb * _LightColor0.a * atten* s.Albedo    ;

            // lighted = s.Albedo;
            // fixed signA = sign(lighted.r)+sign(lighted.g)+sign(lighted.b);
            // fixed colS = sign(_LightColor0.r)+sign(_LightColor0.r)+sign(_LightColor0.r);
            // echoed *=  (1-colS) ;
            //s.Albedo = 1-sign(echoed);
            c.rgb = lighted + echoed ;
            c.rgb *= s.Alpha ;
            // c.rgb = echoed;  //fixed3(signA,signA,signA);

            // c.rgb = sign(s.Albedo);
            c.a = s.Alpha;
            s.atten = atten;
        	return c;
        }

        void vert (inout appdata_full v, out Input o)
        {
        	v.vertex = UnityFlipSprite(v.vertex, _Flip);
        	
            #if defined(PIXELSNAP_ON)
            v.vertex = UnityPixelSnap (v.vertex);
            #endif	

            UNITY_INITIALIZE_OUTPUT(Input, o);
            o.color = v.color * _Color * _RendererColor;
        }

		
        fixed wave(fixed radius , fixed dist)
		{
			return smoothstep(radius , radius - _Thickness, dist) - smoothstep(radius - _Thickness, radius - 2 * _Thickness, dist);
		}

        void surf (Input IN, inout MySurfaceOutput o)
        {
        	fixed4 c = SampleSpriteTexture (IN.uv_MainTex) * IN.color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
            //o.Emission = _EchoColor * echo;

            fixed dist1 = distance(_Center1.xy, IN.worldPos.xy);
            fixed dist2 = distance(_Center2.xy, IN.worldPos.xy);
            
            fixed echo1 = wave(_Radius1,dist1);
			fixed echo2 = wave(_Radius2,dist2);
            
            //o.Gloss = min(echo1+echo2,1);
            o.echo1 = echo1;
            o.echo2 = echo2;
        }

		ENDCG

		
    }

Fallback "Diffuse"
}
