﻿Shader "Sprites/Custom/EcholocationOneNormal" {
	
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
		_BumpMap ("Normalmap", 2D) = "bump" {}
		_BumpIntensity ("NormalMap Intensity", Range (-1, 2)) = 1
        _EchoColor("EchoColor", Color) = (1,1,1,1)
        _Center("Center", vector) = (0,0,0)
        _Radius("Radius",Float) = 1
        _Thickness("Thickness",Float) = .5

        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf PointLambert vertex:vert nofog nolightmap nodynlightmap keepalpha noinstancing
        #pragma multi_compile _ PIXELSNAP_ON
        #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
        #include "UnitySprites.cginc"

        fixed4 _EchoColor;
        fixed3 _Center;
        fixed _Radius;
        fixed _Thickness;
		sampler2D _BumpMap;
		fixed _BumpIntensity;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_BumpMap;
            fixed4 color;
            float3 worldPos;
			//fixed3 radiusScaled;
        };

        struct MySurfaceOutput
        {
            fixed3 Albedo;
            fixed3 Normal;
            fixed3 Emission;
            half Specular;
            fixed Gloss;
            fixed Alpha;
            fixed echo;
            fixed3 centToPoint;
        };


		//FINAL COLOR Maybe
        half4 LightingPointLambert(MySurfaceOutput s, half3 lightDir, half atten) {
        	fixed4 c;
			fixed NdotL = dot(s.Normal,lightDir);
			fixed3 lighted = _LightColor0.rgb * _LightColor0.a * NdotL;
            
			fixed3 echoed = _EchoColor * s.echo * _EchoColor.a;
			echoed *= dot(s.Normal, normalize(s.centToPoint)) * _BumpIntensity;

            c.rgb = echoed * (1-lighted) + lighted;
            c.rgb *= atten * s.Alpha ;
			c.a = s.Alpha;
        	return c;
        }

        void vert (inout appdata_full v, out Input o)
        {
        	v.vertex = UnityFlipSprite(v.vertex, _Flip);
        	
            #if defined(PIXELSNAP_ON)
            v.vertex = UnityPixelSnap (v.vertex);
            #endif	
			v.normal = float3(0,0,-1);
			v.tangent = float4(1,0,0,1);
            UNITY_INITIALIZE_OUTPUT(Input, o);
            o.color = v.color * _Color * _RendererColor;
        }

		
        fixed wave(fixed radius , fixed dist)
		{
			return smoothstep(_Radius, _Radius - _Thickness, dist);;
		}

        void surf (Input IN, inout MySurfaceOutput o)
        {
        	fixed4 c = SampleSpriteTexture (IN.uv_MainTex) * IN.color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
            o.Normal = UnpackNormal(tex2D(_BumpMap,IN.uv_BumpMap));
			fixed dist = distance(_Center.xy, IN.worldPos.xy);
            fixed echo = wave(_Radius,dist);
			
			o.centToPoint = _Center - IN.worldPos;
            o.echo = echo;
        }
		ENDCG	
    }
	FallBack "Diffuse"
}
