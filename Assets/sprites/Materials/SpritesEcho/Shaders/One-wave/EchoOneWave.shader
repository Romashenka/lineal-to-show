﻿Shader "Sprites/Custom/EcholocationOneWave" {
	
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
		_EchoColor("EchoColor", Color) = (1,1,1,1)
        _Center("Center", vector) = (0,0,0)
        _Radius("Radius",Float) = 1
        _Thickness("Thickness",Float) = .5
        _SectorAngle("Sector angle (rad)", Float) = 3

        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf PointLambert vertex:vert nofog nolightmap nodynlightmap keepalpha noinstancing
        #pragma multi_compile _ PIXELSNAP_ON
        #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
        #include "UnitySprites.cginc"

        fixed4 _EchoColor;
        fixed4 _Center;
        fixed _Radius;
        fixed _Thickness;
        fixed _SectorAngle;
		
        struct Input
        {
            float2 uv_MainTex;
		    fixed4 color;
            float3 worldPos;
			//fixed3 radiusScaled;
        };

        struct MySurfaceOutput
        {
            fixed3 Albedo;
            fixed3 Normal;
            fixed3 Emission;
            half Specular;
            fixed Gloss;
            fixed Alpha;
            fixed echo;
        };


		half4 LightingPointLambert(MySurfaceOutput s, half3 lightDir, half atten) {
        	fixed4 c;
			fixed3 lighted = _LightColor0.rgb * _LightColor0.a * s.Albedo;
            
			fixed3 echoed = _EchoColor * s.echo * _EchoColor.a;
			
            c.rgb = echoed  + lighted;
            c.rgb *= atten * s.Alpha;
			c.a = s.Alpha;
        	return c;
        }

        void vert (inout appdata_full v, out Input o)
        {
        	v.vertex = UnityFlipSprite(v.vertex, _Flip);
        	
            #if defined(PIXELSNAP_ON)
            v.vertex = UnityPixelSnap (v.vertex);
            #endif	
			UNITY_INITIALIZE_OUTPUT(Input, o);
            o.color = v.color * _Color * _RendererColor;
			
        }

		
        fixed wave(fixed radius , fixed dist, fixed thickness)
		{
			return smoothstep(radius , radius - thickness, dist) - smoothstep(radius - thickness, radius - 2 * thickness, dist);
		}

        fixed frustrum(fixed3 centerToPoint, fixed centerRotation,fixed sectorAngle, fixed blurThickness)
        {
            fixed rotPlusSector = (centerRotation + sectorAngle/2);
            fixed2 left = fixed2(cos(rotPlusSector),sin(rotPlusSector));
            fixed rotMinusSector = (centerRotation - sectorAngle/2);
            fixed2 right = fixed2(cos(rotMinusSector),sin(rotMinusSector));

            fixed dotL = dot(centerToPoint,left);
            fixed dotR = dot(centerToPoint,right);
            
            fixed myBool = min(sign(dotL),0) * max(sign(dotR),0);
            return (smoothstep(-blurThickness,0 , dotL) - smoothstep(0,blurThickness , dotR)) * myBool;
        }

        void surf (Input IN, inout MySurfaceOutput o)
        {
        	fixed4 c = SampleSpriteTexture (IN.uv_MainTex) * IN.color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
            
            fixed dist = distance(_Center.xy, IN.worldPos.xy);
            fixed3 centToPoint = _Center.xyz - IN.worldPos.xyz;
            fixed echo = wave(_Radius,dist, _Thickness); //* frustrum(centToPoint, _Center.w, _SectorAngle, _Thickness);
			o.echo = echo;
        }
		ENDCG	
    }
Fallback "Diffuse"
}
