﻿Shader "Sprites/Custom/EcholocationWave Normal" {
	
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
		_BumpMap ("Normalmap", 2D) = "bump" {}
		_BumpIntensity ("NormalMap Intensity", Range (-1, 2)) = 1
        _EchoColor("EchoColor", Color) = (1,1,1,1)
        _EchoAlpha1("EchoAlpha1", Range(0,1)) = 1
        _EchoAlpha2("EchoAlpha2", Range(0,1)) = 1
        _Center1("Center1", vector) = (0,0,0)
        _Center2("Center2", vector) = (0,0,0)
        _Radius1("Radius1",Float) = 1
        _Radius2("Radius2", float) = 1
        _Thickness("Thickness",Float) = .5

        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf PointLambert vertex:vert nofog nolightmap nodynlightmap keepalpha noinstancing
        #pragma multi_compile _ PIXELSNAP_ON
        #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
        #include "UnitySprites.cginc"

        fixed4 _EchoColor;
        fixed _EchoAlpha1;
        fixed _EchoAlpha2;
        fixed3 _Center1;
        fixed3 _Center2;
        fixed _Radius1;
        fixed _Radius2;
        fixed _Thickness;
		sampler2D _BumpMap;
		fixed _BumpIntensity;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_BumpMap;
            fixed4 color;
            float3 worldPos;
			//fixed3 radiusScaled;
        };

        struct MySurfaceOutput
        {
            fixed3 Albedo;
            fixed3 Normal;
            fixed3 Emission;
            half Specular;
            fixed Gloss;
            fixed Alpha;
            fixed echo1;
            fixed echo2;
			fixed3 centToPoint1;
			fixed3 centToPoint2;
        };


		//FINAL COLOR Maybe
        half4 LightingPointLambert(MySurfaceOutput s, half3 lightDir, half atten) {
        	fixed4 c;
			fixed NdotL = dot(s.Normal,lightDir);
			fixed echoA1 = _EchoAlpha1 * s.echo1;
            fixed echoA2 = _EchoAlpha2 * s.echo2;
            fixed echoAlpha = max(echoA1,echoA2);
            fixed3 echoed = _EchoColor * echoAlpha * _EchoColor.a;
			echoed *= dot(s.Normal, normalize(s.centToPoint1 * (echoA1 + (1-sign(echoA1))*fixed3(1,1,1)) + 
            s.centToPoint2 *(echoA1 + (1-sign(echoA2)* fixed3(1,1,1)))));
            fixed3 lighted = _LightColor0.rgb * _LightColor0.a * NdotL;
            c.rgb = echoed * (1-lighted) + lighted;
            c.rgb *= atten * s.Alpha ;
			c.a = s.Alpha;
        	return c;
        }

        void vert (inout appdata_full v, out Input o)
        {
        	v.vertex = UnityFlipSprite(v.vertex, _Flip);
        	
            #if defined(PIXELSNAP_ON)
            v.vertex = UnityPixelSnap (v.vertex);
            #endif	
			v.normal = float3(0,0,-1);
			v.tangent = float4(1,0,0,1);
            UNITY_INITIALIZE_OUTPUT(Input, o);
            o.color = v.color * _Color * _RendererColor;
			
        }

		
        fixed wave(fixed radius , fixed dist)
		{
			return smoothstep(radius , radius - _Thickness, dist) - smoothstep(radius - _Thickness, radius - 2 * _Thickness, dist);
		}

        void surf (Input IN, inout MySurfaceOutput o)
        {
        	fixed4 c = SampleSpriteTexture (IN.uv_MainTex) * IN.color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
            //o.Emission = _EchoColor * echo;
			o.Normal = UnpackNormal(tex2D(_BumpMap,IN.uv_BumpMap));
			//o.Normal.z *= 1/_BumpIntensity;
            //o.Normal.z = normalize((fixed)o.Normal);
			fixed dist1 = distance(_Center1.xy, IN.worldPos.xy);
            fixed dist2 = distance(_Center2.xy, IN.worldPos.xy);

			//o.centToPoint1 = (_Center1.x - IN.worldPos.x,_Center1.y-IN.worldPos.y,_Center1.z-IN.worldPos.z);
			
            
            
            //fixed z = (_Radius1 - _Thickness) * _Center1.z;
			// fixed3 radiusWorldPos = fixed3(normalize(IN.worldPos - _Center1) * (_Radius1 - _Thickness));
            //radiusWorldPos.z = _Center1.z;
			//o.radiusToPoint1 = fixed3(_Center1.xy,0) + radiusWorldPos - IN.worldPos; //(radiusWorldPos.xy - IN.worldPos,_Center1.z - IN.worldPos.z); 
			//fixed3 centToRad = (_Center1.x + _Radius1,_Center1.y + _Radius1,_Center1.z);
			
            fixed echo1 = wave(_Radius1,dist1);
			fixed echo2 = wave(_Radius2,dist2);
            o.centToPoint1 = _Center1 - IN.worldPos;
            o.centToPoint2 = _Center2 - IN.worldPos;
            //o.Gloss = min(echo1+echo2,1);
            o.echo1 = echo1;
            o.echo2 = echo2;
        }

		ENDCG	
    }
	
Fallback "Diffuse"
}
