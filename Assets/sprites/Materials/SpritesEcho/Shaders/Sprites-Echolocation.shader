﻿Shader "Sprites/Custom/Sprites-Echolocation" {

    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)

            _EchoColor("EchoColor", Color) = (1,1,1,1)
            _EchoAlpha1("EchoAlpha1", Range(0,1)) = 1
            _EchoAlpha2("EchoAlpha2", Range(0,1)) = 1
            _Center1("Center1", vector) = (0,0,0)
            _Center2("Center2", vector) = (0,0,0)
            _Radius1("Radius1",Float) = 1
            _Radius2("Radius2", float) = 1
            _Thickness("Thickness",Float) = .5

        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf PointLambert vertex:vert nofog nolightmap nodynlightmap keepalpha noinstancing
        #pragma multi_compile _ PIXELSNAP_ON
        #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
        #include "UnitySprites.cginc"

        fixed4 _EchoColor;
        fixed _EchoAlpha1;
        fixed _EchoAlpha2;
        fixed3 _Center1;
        fixed3 _Center2;
        fixed _Radius1;
        fixed _Radius2;
        float _Thickness;

        
        struct Input
        {
            float2 uv_MainTex;
            fixed4 color;
            float3 worldPos;
        };

        struct MySurfaceOutput
        {
            fixed3 Albedo;
            fixed3 Normal;
            fixed3 Emission;
            half Specular;
            fixed Gloss;
            fixed Alpha;
            fixed echo1;
            fixed echo2;
        };

        half4 LightingPointLambert(MySurfaceOutput s, half3 lightDir, half atten) {
        	fixed4 c;
        	
            //fixed echo = saturate(s.echo1 + s.echo2);
        	//fixed4 EchoC1 = EchoColor1 * echo1;
            //fixed4 EchoC2 = EchoColor2 * echo2;
            fixed echoA1 = _EchoAlpha1 * s.echo1;
            fixed echoA2 = _EchoAlpha2 * s.echo2;
            fixed echoAlpha = max(echoA1,echoA2);
            fixed3 lighted;
            lighted.rgb = _LightColor0.rgb;
            fixed3 echoed = _EchoColor * echoAlpha * _EchoColor.a;
            c.rgb = echoed * (lighted == 0) + lighted * _LightColor0.a;
            //c.rgb = sign(lighted);
            
            c.rgb *= atten * s.Alpha * s.Albedo;
            c.a = s.Alpha;
        	return c;
        }

        void vert (inout appdata_full v, out Input o)
        {
        	v.vertex = UnityFlipSprite(v.vertex, _Flip);
        	
            #if defined(PIXELSNAP_ON)
            v.vertex = UnityPixelSnap (v.vertex);
            #endif	

            UNITY_INITIALIZE_OUTPUT(Input, o);
            o.color = v.color * _Color * _RendererColor;
        }

        void surf (Input IN, inout MySurfaceOutput o)
        {
        	fixed4 c = SampleSpriteTexture (IN.uv_MainTex) * IN.color;
            o.Albedo = c.rgb;
            o.Alpha = c.a;
            //o.Emission = _EchoColor * echo;

            fixed dist1 = distance(_Center1.xy, IN.worldPos.xy);
            fixed dist2 = distance(_Center2.xy, IN.worldPos.xy);
            
            fixed echo1 = smoothstep(_Radius1, _Radius1 - _Thickness, dist1);
            fixed echo2 = smoothstep(_Radius2, _Radius2 - _Thickness, dist2);
            
            //o.Gloss = min(echo1+echo2,1);
            o.echo1 = echo1;
            o.echo2 = echo2;
        }
        ENDCG
    }

Fallback "Diffuse"
}
