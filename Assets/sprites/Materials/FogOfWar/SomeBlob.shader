﻿Shader "Sprites/Custom/SomeBlob" {
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Thickness("Thickness",Float) = .5
		_Color("Color", Color) = (1,1,1)
	}
	SubShader
	{
		Tags 
		{ 
			"RenderType"="Transparent"
			"IgnoreProjector"="True"
			"PreviewType"="Plane"
			"Queue" = "Transparent"
		}
		Cull Off
        // Lighting Off
        // ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// #pragma keepalpha

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed _Thickness;
			fixed3 _Color;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed wave(fixed radius, fixed thickness, fixed dist)
			{
				return smoothstep(radius, radius - thickness, dist);;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4  col;
				fixed2 center = fixed2(.5,.5);
				fixed dist = distance(i.uv,center);
				col.rgb = _Color.rgb;
				col.a = wave(.5, _Thickness, dist);
				return col;
			}
			ENDCG
		}
	}
}
