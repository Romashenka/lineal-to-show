﻿using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Component, that activate-deactivate gameObject.
    /// </summary>
    public class OnOffGameObject : MonoBehaviour, IOnOff
    {
        public void SetOnOff(bool active) { gameObject.SetActive(active); }
    }
}