﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace BatProject
{
    /// <summary>
    /// Component, that allows UI-gameObject to follow the target's world position.
    /// </summary>
    public class ObjectFollowerUI : MonoBehaviour
    {

        public Transform target;

        RectTransform rect;

        void Start()
        {
            rect = GetComponent<RectTransform>();
        }

        void Update()
        {
            Vector2 targetViewportPos = Camera.main.WorldToScreenPoint(target.position);
            rect.position = targetViewportPos;
        }
    }
}