﻿using UnityEngine;

public class ObjectInfo : ScriptableObject
{
    public Sprite image;
    public string description;
}
