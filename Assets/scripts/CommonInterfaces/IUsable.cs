﻿using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Common interface for usable items, located in scene.
    ///</summary>
    public interface IUsable
    {
        void Use(Player.PlayerController user);
        void OnDetected();
        void OnLost();
    }
}