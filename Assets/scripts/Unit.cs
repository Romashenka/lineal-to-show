﻿using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Base abstract component that should be on all living-like gameObjects in game.
    /// </summary>
    public abstract class Unit : MonoBehaviour
    {
        public abstract IUnitDynamicStats statsDynamic { get; }

        virtual public void GetDamage(float dmg)
        {
            statsDynamic.health -= dmg;
            if (statsDynamic.health <= 0)
            {
                Die();
            }
        }
        abstract public void GetImpact(Vector2 point, Vector2 direction);

        abstract protected void Die();
    }
}