﻿using UnityEngine;

namespace BatProject.UI
{
    public class PauseLayer : UILayer
    {
        public override void SetActive(bool active)
        {
            Time.timeScale = active ? 0f : 1f;
            GlobalEvents.PauseGame(active);
            base.SetActive(active);
        }
    }
}