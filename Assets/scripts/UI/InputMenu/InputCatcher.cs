﻿using UnityEngine;


namespace BatProject.UI
{
    public class InputCatcher : MonoBehaviour
    {
        InputButton changingButton;

        void Update()
        {
            CustomInput current = InputControl.currentInput();
            if (current != null && changingButton != null)
            {
                KeyMapping keyMapping = InputControl.setKey(changingButton.controlName, current);
                changingButton.TakeLabels(keyMapping);
                gameObject.SetActive(false);
            }
        }

        public void WaitForNewInput(InputButton sender)
        {
            gameObject.SetActive(true);
            changingButton = sender;
        }
    }
}