﻿using System;
using UnityEngine;
using UnityEngine.UI;


namespace BatProject.UI
{
    public class InputButton : MonoBehaviour
    {
        [SerializeField]
        private Text controlLabel;

        [SerializeField]
        private Text keyLabel;
        public string controlName { get { return controlLabel.text; } }

        public Action buttonClicked;

        public void TakeLabels(KeyMapping key)
        {
            controlLabel.text = key.name;
            keyLabel.text = key.primaryInput.ToString();
        }

        public void ChangeKeyLabel(string newLabel)
        {
            keyLabel.text = newLabel;
        }

        public void ClickButton()
        {
            if (buttonClicked != null)
                buttonClicked();
        }


        void OnDestroy() { buttonClicked = null; }
    }
}