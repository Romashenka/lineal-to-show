﻿using System.Collections.ObjectModel;
using System.Collections.Generic;
using UnityEngine;

namespace BatProject.UI
{
    [CreateAssetMenu]
    public class GameControls : ScriptableObject
    {
        public ReadOnlyCollection<KeyMapping> defaultKeys;
        public ReadOnlyCollection<KeyMapping> customedKeys;
    }
}
