﻿using System;
using System.Collections.ObjectModel;
using UnityEngine;
using BatProject.Items;

namespace BatProject.UI
{
    /// <summary>
    /// Component that controlls all input.
    /// </summary>
    public class InputController : MonoBehaviour
    {
        public Action inventoryOpen;
        public Action inventoryClose;
        public Action use;
        public Action showCheckpointTracker;
        public Action hideCheckpointTracker;

        private bool inGameState = true;
        void Start()
        {
            GlobalEvents.gamePaused += (bool paused) => inGameState = !paused;
        }

        void Update()
        {
            if (inGameState)
                HandleUserInput();
        }

        void HandleUserInput()
        {
            if (InputControl.GetButtonDown(Controls.buttons.use))
            {
                if (use != null)
                {
                    use();
                }
            }

            if (InputControl.GetButtonDown(Controls.buttons.showCheckpointTracker))
            {
                if (showCheckpointTracker != null)
                {
                    showCheckpointTracker();
                }
            }

            if (InputControl.GetButtonUp(Controls.buttons.showCheckpointTracker))
            {
                if (hideCheckpointTracker != null)
                {
                    hideCheckpointTracker();
                }
            }
        }

        // void HandleUserInputFU()
        // {
        //     float horizontalMove = Input.GetKey(KeyCode.D) ? 1f : 0f;
        //     horizontalMove += Input.GetKey(KeyCode.A) ? -1f : 0f;
        //     float verticalMove = Input.GetKey(KeyCode.W) ? 1f : 0f;
        //     verticalMove += Input.GetKey(KeyCode.S) ? -1f : 0f;
        //     moveInputVector = new Vector2(horizontalMove, verticalMove);
        // }

        void OnDisable()
        {
            GlobalEvents.gamePaused -= (bool pause) => inGameState = pause;
        }
    }
}