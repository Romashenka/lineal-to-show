﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace BatProject.UI
{
    public class InputMenu : MonoBehaviour
    {
        [SerializeField]
        private InputButton buttonPrefab;

        [SerializeField]
        private InputCatcher inputCatcher;

        private const string notIncluded = "escape";

        private List<InputButton> instantiatedButtons;

        void Awake()
        {
            Controls.init();
        }

        void OnEnable()
        {
            instantiatedButtons = new List<InputButton>(10);
            if (GetComponentInChildren<InputButton>() == null)
            {
                ReadOnlyCollection<KeyMapping> keys = InputControl.getKeysList();
                int keysLen = keys.Count;
                for (int i = 0; i < keysLen; i++)
                {
                    if (keys[i].name == notIncluded)
                        continue;

                    InputButton button = Instantiate(buttonPrefab, transform);
                    button.TakeLabels(keys[i]);
                    button.buttonClicked += () => inputCatcher.WaitForNewInput(button);
                    instantiatedButtons.Add(button);
                }
            }
        }

        void OnDisable()
        {
            foreach (var button in instantiatedButtons)
            {
                Destroy(button.gameObject);
            }
            instantiatedButtons = null;
        }

        /// <summary>
        /// Resets input to defaults and closes window
        /// </summary>
        public void ResetInput()
        {
            Controls.loadDefaults();
            
        }
    }
}