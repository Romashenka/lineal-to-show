﻿using UnityEngine;


namespace BatProject.UI
{
    public class UILayer : MonoBehaviour
    {
        public bool isActive { get { return gameObject.activeSelf; } }
        public virtual void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
    }
}