﻿using UnityEngine;
using System;

namespace BatProject
{
    public static class GlobalEvents
    {
        /// <summary>
        /// Invokes when saving system have loaded all savable gameobjects.
        /// </summary>
        public static event Action gameLoaded;
        /// <summary>
        /// Invokes when player have died.
        /// </summary>
        public static event Action playerDead;
        /// <summary>
        /// Invokes when game have been paused.
        /// </summary>
        public static event Action<bool> gamePaused;
        /// <summary>
        /// Invokes when appropriate method called.
        /// </summary>
        public static event Action<bool> blockUserInput;


        public static void PauseGame(bool pause)
        {
            if (gamePaused != null)
            {
                gamePaused(pause);
            }
        }

        public static void PlayerDead()
        {
            if (playerDead != null)
            {
                playerDead();
            }
        }

        public static void GameLoaded()
        {
            if (gameLoaded != null)
            {
                gameLoaded();
            }
        }

        public static void BlockUserInput(bool block)
        {
            if (blockUserInput != null)
            {
                blockUserInput(block);
            }
        }
    }
}