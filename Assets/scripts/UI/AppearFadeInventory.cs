﻿

namespace BatProject.UI
{
    /// <summary>
    /// Component that controlls appear and fade logic for inventory.
    /// </summary>
    public class AppearFadeInventory : AppearFade
    {

        public InputController controller;

        public override void Start()
        {
            base.Start();
            controller.inventoryOpen += Appear;
            controller.inventoryClose += Fade;
        }
    }
}