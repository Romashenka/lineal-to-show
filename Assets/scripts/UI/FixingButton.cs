﻿using UnityEngine;
using UnityEngine.UI;

namespace BatProject.UI
{
    [RequireComponent(typeof(Button), typeof(Image))]
    public class FixingButton : MonoBehaviour
    {
        public UILayerWithCallback targetLayer;
        public Color onTargActiveColor;
        private Button button;
        private ColorBlock defaultButtonColors;
        void Awake()
        {
            button = GetComponent<Button>();
            targetLayer.activityChanged += OnTargetStateChanged;
            defaultButtonColors = button.colors;
        }

        void OnTargetStateChanged(UILayer target)
        {
            if (target.isActive)
            {
                ColorBlock colors = defaultButtonColors;
                colors.disabledColor = onTargActiveColor;
                button.colors = colors;
                button.interactable = false;
            }
            else
            {
                button.colors = defaultButtonColors;
                button.interactable = true;
            }
        }

    }
}