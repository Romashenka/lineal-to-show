﻿
namespace BatProject.UI
{
    public class UILayerWithCallback : UILayer
    {
        public event System.Action<UILayer> activityChanged;

        override public void SetActive(bool active)
        {
            base.SetActive(active);
            if (activityChanged != null)
                activityChanged(this);
        }
    }
}