﻿using UnityEngine;

namespace BatProject.UI
{
    public class ScaleWithMouseWheel : MonoBehaviour
    {
        public float maxScale = 3f;
        public float minScale = 1f;
        private Vector2 startPos;
        private RectTransform rectTransform;

        void Start()
        {
            rectTransform = GetComponent<RectTransform>();
            startPos = rectTransform.position;
        }

        void Update()
        {
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                float scaleDelta =
                    Mathf.Clamp(Input.GetAxis("Mouse ScrollWheel"), minScale - transform.localScale.x, maxScale - transform.localScale.x);
                rectTransform.localScale += Vector3.one * scaleDelta;
                Vector2 mouseOnThis = Input.mousePosition - (Vector3)startPos;
                rectTransform.anchoredPosition -= mouseOnThis * scaleDelta;
            }
        }

    }
}