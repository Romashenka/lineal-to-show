﻿using UnityEngine.UI;
using UnityEngine;

namespace BatProject.UI
{
    public class ButtonsFixer : MonoBehaviour
    {
        public Color pressedButtonCollor = Color.green;
        private Button pressedButton;
        private ColorBlock pressedButtonDefault;

        void Awake()
        {
            SetChildrenButtons();
        }

        public void SetChildrenButtons()
        {
            Button[] childrenButtons = GetComponentsInChildren<Button>(true);

            int length = childrenButtons.Length;
            for (int i = 0; i < length; i++)
            {
                Button b = childrenButtons[i];
                b.onClick.AddListener(() => PressButton(b));
            }
            
        }


        void PressButton(Button button)
        {
            if (pressedButton != null)
            {
                pressedButton.colors = pressedButtonDefault;
                pressedButton.interactable = true;
            }
            pressedButtonDefault = button.colors;
            ColorBlock newColBl = pressedButtonDefault;
            newColBl.disabledColor = pressedButtonCollor;
            button.colors = newColBl;
            pressedButton = button;
            button.interactable = false;
        }
    }
}