﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace BatProject.UI
{

    /// <summary>
    /// UI-component that shows some other gameObject OnPointerDown and hide it OnPointerUp.
    /// </summary>
    public class UIShowOnPush : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public GameObject objToShow;
        public void OnPointerDown(PointerEventData eventData)
        {
            objToShow.SetActive(true);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            objToShow.SetActive(false);
        }
    }
}