﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BatProject.UI
{
    public class FogOfWar : MonoBehaviour
    {
        public float maxAppertureSize = 50f;
        public LayerMask fOWLayer;
        public RenderTexture fOWTexture;
        public Camera minimapCamera;
        public GameObject pictureToRender;
        public Echo.EchoMotor motor;
        public UILayerWithCallback mapPanel;

        private Stack<Vector2> echoPoints;


        void Start()
        {
            echoPoints = new Stack<Vector2>();
            motor.echoCycleStarted += (Vector2 pos) => echoPoints.Push(pos);
            mapPanel.activityChanged += OnMapActivityChanged;
            ClearFOWTexture();
        }

        void OnMapActivityChanged(UILayer map)
        {
            if (!map.isActive || echoPoints.Count == 0)
                return;

            CameraSettings defaultCamSet = new CameraSettings(minimapCamera);

            CameraSettings.SetToCamera(minimapCamera, fOWLayer, CameraClearFlags.Nothing, fOWTexture);

            GameObject[] picsToRend = new GameObject[echoPoints.Count];

            Vector2 lastEchoPoint = echoPoints.Pop();
            picsToRend[0] = SetApperture(lastEchoPoint, maxAppertureSize * 2f * motor.echoParams.phasePercent);

            for (int i = 1; i < picsToRend.Length; i++)
            {
                picsToRend[i] = SetApperture(echoPoints.Pop(), maxAppertureSize * 2f);
            }

            minimapCamera.Render();

            foreach (GameObject go in picsToRend)
            {
                Destroy(go);
            }

            echoPoints.Push(lastEchoPoint);

            CameraSettings.SetToCamera(minimapCamera, defaultCamSet);
        }


        void ClearFOWTexture()
        {
            CameraSettings defaultCamSet = new CameraSettings(minimapCamera);
            CameraSettings.SetToCamera(minimapCamera, fOWLayer, CameraClearFlags.Color, fOWTexture);
            minimapCamera.Render();
            CameraSettings.SetToCamera(minimapCamera, defaultCamSet);
        }

        GameObject SetApperture(Vector2 position, float size)
        {
            GameObject appert = Instantiate(pictureToRender, position, Quaternion.identity);
            appert.transform.localScale = new Vector3(size, size, size);
            return appert;
        }

        private struct CameraSettings
        {
            private LayerMask cullingMask;
            private CameraClearFlags clearFlags;
            private RenderTexture targetTexture;

            public CameraSettings(Camera camera)
            {
                cullingMask = camera.cullingMask;
                clearFlags = camera.clearFlags;
                targetTexture = camera.targetTexture;
            }

            /*
            public CameraSettings(int cullingMask, CameraClearFlags clearFlags, RenderTexture targetTexture)
            {
                this.cullingMask = cullingMask;
                this.clearFlags = clearFlags;
                this.targetTexture = targetTexture;
            }
             */

            public static void SetToCamera(Camera camera, CameraSettings settings)
            {
                SetToCamera(camera, settings.cullingMask, settings.clearFlags, settings.targetTexture);
            }

            public static void SetToCamera(Camera camera, int cullingMask, CameraClearFlags clearFlags, RenderTexture targetTexture)
            {
                camera.cullingMask = cullingMask;
                camera.clearFlags = clearFlags;
                camera.targetTexture = targetTexture;
            }
        }
    }
}