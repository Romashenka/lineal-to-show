﻿using UnityEngine;
using UnityEngine.UI;


namespace BatProject.UI
{
    public class BulletCounter : MonoBehaviour
    {
        [SerializeField]
        private Slider clipSlider;

        [SerializeField]
        private Text ammoCountText;

        public int ammoCount { set { ammoCountText.text = value.ToString(); } }
        public int clipSize { set { clipSlider.maxValue = value; } }
        public int clipCurrent { set { clipSlider.value = value; } }
    }
}