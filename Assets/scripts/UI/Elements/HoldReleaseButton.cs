﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace BatProject.UI
{

    /// <summary>
    /// UI-component that describes button with pointer down and pointer up callbacks.
    /// </summary>
    public class HoldReleaseButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public System.Action pointerDown;
        public System.Action pointerUp;

        public void OnPointerDown(PointerEventData eventData)
        {
            if (pointerDown != null)
            {
                pointerDown();
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (pointerUp != null)
            {
                pointerUp();
            }
        }
    }

}