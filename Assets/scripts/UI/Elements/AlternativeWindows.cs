﻿using UnityEngine;

namespace BatProject.UI
{
    public class AlternativeWindows : UILayer
    {
        private GameObject opened;
        
        public void OpenWindow(GameObject windowToOpen)
        {
            if (opened != null)
                opened.SetActive(false);
            windowToOpen.SetActive(true);
            opened = windowToOpen;
        }
    }
}