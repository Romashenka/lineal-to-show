﻿using UnityEngine.UI;
using UnityEngine;

public class UIHighlightableChooseElem<T> : MonoBehaviour {

    private Image image;
    public Color highlighted;
    public T prefabRef;
    public T PrefabRef { get { return prefabRef; } }

    Color defaultCol;
    void Awake()
    {
        image = GetComponent<Image>();
        defaultCol = image.color;
    }

    public void Highlight()
    {
        image.color = highlighted;
    }

    public void ResetHL()
    {
        image.color = defaultCol;
    }

    void OnDisable()
    {
        ResetHL();
    }
}
