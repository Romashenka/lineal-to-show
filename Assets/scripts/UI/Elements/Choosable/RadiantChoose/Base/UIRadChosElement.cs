﻿using UnityEngine;
using UnityEngine.UI;

namespace BatProject.UI.RadiantChoose
{
    /// <summary>
    /// Generic for UI-component that can be highlighted and contains some prefab.
    /// </summary>
    public class UIRadChosElement<T> : MonoBehaviour
    {

        private Image image;
        public Color highlighted;

        [SerializeField]
        private T prefabRef;
        public T PrefabRef { get { return prefabRef; } }

        Color defaultCol;
        void Awake()
        {
            image = GetComponent<Image>();
            defaultCol = image.color;
        }

        public void Highlight()
        {
            image.color = highlighted;
        }

        public void ResetHL()
        {
            image.color = defaultCol;
        }

        void OnDisable()
        {
            ResetHL();
        }
    }
}