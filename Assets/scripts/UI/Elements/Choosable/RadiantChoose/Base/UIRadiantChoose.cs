﻿using BatProject.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BatProject.UI.RadiantChoose
{
    /// <summary>
    /// Generic for UI-component that handles input for <see cref="UIHighlightableChooseElem"/> 
    ///and have callback on some <see cref="UIRadChosElement"/> chosen.
    /// </summary>
    [RequireComponent(typeof(RoundNavigationUI))]
    public class UIRadiantChoose<T> : MonoBehaviour, IPointerUpHandler, IDragHandler
    {
        public GameObject elementsPlane;
        public System.Action<T> itemChosen;
        UIRadChosFinder<T> finder;

        T currentItem;

        void Start()
        {
            finder = new UIRadChosFinder<T>(gameObject);
        }

        public void OnDrag(PointerEventData eventData)
        {
            T t = finder.DefineItemOnPosition(eventData.position);
            currentItem = t;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (itemChosen != null)
                itemChosen(currentItem);
        }
    }
}