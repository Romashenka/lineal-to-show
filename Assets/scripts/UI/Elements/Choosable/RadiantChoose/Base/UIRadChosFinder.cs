﻿using UnityEngine;


namespace BatProject.UI.RadiantChoose
{

    /// <summary>
    /// Generic for UI-component that can find <see cref="UIHighlightableChooseElem"/> and retun it.
    /// </summary>
    public class UIRadChosFinder<T>
    {
        private RectTransform myRect;
        private int lastChosenButtonIndex = -1;

        private UIHighlightableChooseElem<T>[] elems;

        public UIRadChosFinder(GameObject parent)
        {
            myRect = parent.GetComponent<RectTransform>();
            elems = parent.GetComponentsInChildren<UIHighlightableChooseElem<T>>(true);
            if (elems.Length == 0)
            {
                Debug.Log(string.Format("No children with type {0}", typeof(UIHighlightableChooseElem<T>)), parent);
            }
        }


        public T DefineItemOnPosition(Vector2 position)
        {
            float angle =
                Vector2.SignedAngle(Vector2.right, position - (Vector2)myRect.position) + myRect.eulerAngles.z + 180f / elems.Length;
            if (angle < 0f)
                angle += 360f;
            int chosenIndex = (int)(angle * (elems.Length / 360f));

            if (lastChosenButtonIndex != chosenIndex)
            {
                if (lastChosenButtonIndex != -1)
                    elems[lastChosenButtonIndex].ResetHL();
                elems[chosenIndex].Highlight();
                lastChosenButtonIndex = chosenIndex;
            }
            return elems[chosenIndex].prefabRef;
        }
    }
}