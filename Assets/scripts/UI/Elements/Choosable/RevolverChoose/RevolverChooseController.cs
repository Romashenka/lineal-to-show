﻿using UnityEngine;
using BatProject.Items;

namespace BatProject.UI
{
    /// <summary>
    /// Controlls highlighting of <see cref="RevolverChamber"/>s and have event that invokes when it swithced and informs for what.
    /// </summary>
    public class RevolverChooseController : MonoBehaviour
    {
        public Revolver revolver;
        public RevolverChamber[] buttons;
        private int highlightedButtonIndex;
        /// <summary>
        /// Invokes when <see cref="RevolverChamber"/> switched and informs for what weapon.
        /// </summary>
        public System.Action<Weapon> switched;

        void Start()
        {
            if (revolver == null)
                revolver = GetComponent<Revolver>();
            if (revolver == null)
            {
                Debug.Log("No revolver for me.", this);
                Destroy(this);
                return;
            }

            if (buttons == null && buttons.Length == 0)
            {
                buttons = revolver.GetComponentsInChildren<RevolverChamber>(true);
                if (buttons.Length == 0)
                {
                    Debug.Log("No buttons in my children and no assigned in inspector.", this);
                    Destroy(this);
                    return;
                }
            }

            revolver.chamberSwitched += SwitchButton;
            buttons[highlightedButtonIndex].Highlight();
        }

        
        void SwitchButton(int newIndex)
        {
            buttons[highlightedButtonIndex].ResetHL();
            buttons[newIndex].Highlight();
            highlightedButtonIndex = newIndex;
            if (switched != null)
                switched(buttons[newIndex].prefabRef);
        }

        void OnDrawGizmos()
        {
            if (buttons != null && buttons.Length != 0)
            {
                if (buttons[0] != null)
                    Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(buttons[0].transform.position, 10f);
            }
        }
    }
}