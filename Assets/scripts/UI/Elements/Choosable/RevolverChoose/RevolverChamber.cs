﻿
namespace BatProject.UI
{
    /// <summary>
    /// Component, that contains prefab of <see cref="Weapon"/> and can highlight Image.
    /// </summary>
    public class RevolverChamber : UIHighlightableChooseElem<BatProject.Items.Weapon> { }
}