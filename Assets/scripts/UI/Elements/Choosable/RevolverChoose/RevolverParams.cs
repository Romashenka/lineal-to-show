﻿using UnityEngine;


namespace BatProject.UI
{
    /// <summary>
    /// <see cref="Revolver"/>'s data-containing ScriptableObject. 
    /// </summary>
    [CreateAssetMenu]
    public class RevolverParams : ScriptableObject
    {
        [SerializeField]
        private float _shiftSpeed = 5f;
        /// <summary>
        /// Speed of rotation.
        /// </summary>
        public float shiftSpeed { get { return _shiftSpeed; } }

        [SerializeField]
        [Range(0f, 5f)]
        private float _shiftingIntensity;
        /// <summary>
        /// Intensity of teeth-like pattern. 
        /// </summary>
        public float shiftingIntensity { get { return _shiftingIntensity; } }

        [SerializeField]
        [Range(0f, 10f)]
        private int _numberOfChambers;
        /// <summary>
        /// Number of teeth in teeth-like pattern. 
        /// </summary>
        public float numberOfChambers { get { return _numberOfChambers; } }
    }
}