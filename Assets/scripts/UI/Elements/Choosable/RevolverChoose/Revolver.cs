﻿using UnityEngine;

namespace BatProject.UI
{
    /// <summary>
    /// UI-component that describes logic for changing rotation movement of it's rigidbody in some tooth weel-like pattern.
    /// Has event that notices about what number of tooth of "tooth weel" switched.
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class Revolver : MonoBehaviour
    {
        public RevolverParams myParams;
        /// <summary>
        /// Notices about what number of tooth of "tooth weel" switched.
        /// </summary>
        public System.Action<int> chamberSwitched;
        private Rigidbody2D myRb;
        private float startRotation;
        private int lastChamber;

        void Start()
        {
            if (myParams == null)
            {
                Debug.Log("Where is my params!?", this);
            }
            myRb = GetComponent<Rigidbody2D>();
            startRotation = myRb.rotation;
        }

        void FixedUpdate()
        {
            Shift();
        }

        public void Rotate(float rotationSpeed)
        {
            if (rotationSpeed != 0f)
                myRb.angularVelocity = rotationSpeed;
        }

        /// <summary>
        /// Rotation logic.
        /// </summary>
        void Shift()
        {
            float degPerElem = 360f / (float)myParams.numberOfChambers;
            int currentChamber = (int)Mathf.Round((myRb.rotation - startRotation) / degPerElem);
            float targetRot = (float)currentChamber * degPerElem + startRotation;
            myRb.rotation = Mathf.MoveTowards(myRb.rotation, targetRot, myParams.shiftSpeed * MoveFunc(myRb.rotation) * Time.deltaTime);
            currentChamber = -currentChamber;
            if (currentChamber != lastChamber)
            {

                int numberOfChambers = (int)myParams.numberOfChambers;
                int correctedChamberIndex = (currentChamber % numberOfChambers + numberOfChambers) % numberOfChambers;
                // Debug.Log("Ind = " + currentChamber + " cInd = " + correctedChamberIndex);
                if (chamberSwitched != null)
                    chamberSwitched(correctedChamberIndex);
                lastChamber = currentChamber;
            }
        }

        /// <summary>
        /// Function, that describes tooth-weel-like pattern.
        /// </summary>
        float MoveFunc(float x)
        {
            x /= 360f;
            x *= Mathf.Deg2Rad;
            x *= 7f;
            return Mathf.Abs(Mathf.Sin(x / 2f)) * myParams.shiftingIntensity + 1f;
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, transform.TransformDirection(Vector2.right * 45f));
        }
    }
}