﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace BatProject.UI
{
    /// <summary>
    /// Component, that defines logic for rotating <see cref="Revolver"/>.
    /// </summary>
    public class UIRevolverRotator : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IDragHandler
    {
        /// <summary>
        /// <see cref="Revolver"/> to rotate.
        /// </summary>
        public Revolver revolver;
        /// <summary>
        /// Angle on what, chamber is considered to be defined(chosen).
        /// </summary>
        public float definedAngle;
        /// <summary>
        /// Threshould of defined anfle.
        /// </summary>
        public float defAngThresould = 10f;
        /// <summary>
        /// Radius of circle with center in this.gameObject position, in which nothing would be rotating and choosing.
        /// </summary>
        public float radiusThreshould = 1f;
        /// <summary>
        /// Max speed of <see cref="Revolver"/> rotation.
        /// </summary>
        public float maxRotationSpeed = 5f;

        private Vector2 pointerPosition;
        /// <summary>
        /// Angle of pointer, gameObject position and Vector2.right.
        /// </summary>
        private float pointerAngle;
        /// <summary>
        /// Current rotation speed of Revolver.
        /// </summary>
        private float rotationSpeed;

        void Start()
        {
            if (revolver == null)
                revolver = GetComponent<Revolver>();
            if (revolver == null)
            {
                Debug.Log("No revolver for me.", this);
                Destroy(this);
                return;
            }
        }

        void FixedUpdate()
        {
            revolver.Rotate(rotationSpeed);
        }

        public void OnPointerDown(PointerEventData eventData)
        {

        }
        public void OnDrag(PointerEventData eventData)
        {
            pointerPosition = eventData.position - (Vector2)transform.position;

            if (pointerPosition.sqrMagnitude > radiusThreshould * radiusThreshould)
            {
                pointerAngle = Vector2.SignedAngle(Vector2.right, pointerPosition);
                rotationSpeed = CalculateRotationSpeed();
            }
            else
            {
                rotationSpeed = 0f;
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            rotationSpeed = 0f;
        }

        float CalculateRotationSpeed()
        {
            float conterClockwise = Mathf.InverseLerp(definedAngle + defAngThresould, definedAngle + 2f * defAngThresould, pointerAngle);
            float clockwise = -Mathf.InverseLerp(definedAngle - defAngThresould, definedAngle - 2f * defAngThresould, pointerAngle);
            return (conterClockwise + clockwise) * maxRotationSpeed;
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, radiusThreshould);
            Gizmos.DrawRay(transform.position, pointerPosition);
            Gizmos.color = Color.blue;
            float ang = Mathf.Deg2Rad * definedAngle;
            Vector2 def = new Vector2(Mathf.Cos(ang), Mathf.Sin(ang)) * 100f;
            Gizmos.DrawRay(transform.position, def);
            Gizmos.color = Color.white;
            Gizmos.DrawRay(transform.position, transform.TransformDirection(Vector3.up) * 100f);
        }
    }
}