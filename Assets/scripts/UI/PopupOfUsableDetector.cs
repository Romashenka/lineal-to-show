﻿using UnityEngine;

namespace BatProject.UI
{
    /// <summary>
    /// UI - component of Popup message showing on DetectorOfUsable detects something.
    /// Not finished!!! For test only.
    /// </summary>
    public class PopupOfUsableDetector : MonoBehaviour
    {
        public DetectorOfIUsable detector;

        void Start()
        {
            detector.usableDetected += (IUsable usable) => gameObject.SetActive(true);
            detector.usablesGone += () => gameObject.SetActive(false);
            gameObject.SetActive(false);
        }

        void Update()
        {
            transform.position = Camera.main.WorldToScreenPoint(detector.transform.position);
            if (Input.GetKeyDown(KeyCode.J))
            {
                gameObject.SetActive(false);
            }
        }
    }
}
