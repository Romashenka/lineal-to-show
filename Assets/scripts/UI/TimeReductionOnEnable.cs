﻿using UnityEngine;

namespace BatProject.UI
{

    /// <summary>
    /// Component that slows down time when this' gameObject is enabled and returns it back when it's disabled.
    /// </summary>
    public class TimeReductionOnEnable : MonoBehaviour
    {

        /// <summary>
        /// Time slow down percent.
        /// </summary>
        [Range(0f, 1f)]
        public float timeReduction;

        private float timeScaleReducted;

        void Start()
        {
            timeScaleReducted = 1f / timeReduction;
        }

        void OnEnable()
        {
            Time.timeScale = timeScaleReducted;
            Time.fixedDeltaTime *= timeScaleReducted;
        }

        void OnDisable()
        {
            Time.timeScale = 1f;
            Time.fixedDeltaTime /= timeScaleReducted;
        }
    }
}