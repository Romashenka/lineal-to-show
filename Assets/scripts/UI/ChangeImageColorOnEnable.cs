﻿using UnityEngine;
using UnityEngine.UI;

namespace BatProject.UI
{
    public class ChangeImageColorOnEnable : MonoBehaviour
    {
        public Image targetImage;
        public Color changedColor;
        private Color defaultColor;

        void OnEnable()
        {
            defaultColor = targetImage.color;
            targetImage.color = changedColor;
        }

        void OnDisable()
        {
            targetImage.color = defaultColor;
        }
    }
}