﻿using UnityEngine;
using System;

namespace BatProject.UI
{

    /// <summary>
    /// UI-tool component for positioning some elements in circle.
    /// Not finished!!! For test only.
    /// </summary>
    public class RoundNavigationUI : MonoBehaviour
    {
#if UNITY_EDITOR

        /// <summary>
        /// Elements that must be positioned in circle.
        /// </summary>
        public RectTransform[] transformsOnRadius;


        /// <summary>
        /// Radius of circle.
        /// </summary>
        public float radius = 5f;

        /// <summary>
        /// Rotation of cirle with elements.
        /// </summary>
        public float rotation;

        RectTransform myTransform;
        public void SetRound()
        {
            if (transformsOnRadius.Length == 0)
                return;

            if (myTransform == null)
            {
                myTransform = GetComponent<RectTransform>();
            }

            float angleStep = 360f / transformsOnRadius.Length * Mathf.Deg2Rad;
            float angle = (myTransform.eulerAngles.z + rotation) * Mathf.Deg2Rad;
            foreach (var transform in transformsOnRadius)
            {
                Vector2 direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
                transform.position = (Vector2)myTransform.position + direction * radius;
                angle += angleStep;
            }
        }
#endif
    }
}