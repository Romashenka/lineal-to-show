﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace BatProject.UI
{
    /// <summary>
    ///UI - component that slowly show some other elements when pointer down on this and hide them when pointer up.
    /// </summary>
    public class AppearFadeOnPointer : AppearFade, IPointerDownHandler, IPointerUpHandler
    {
        public void OnPointerDown(PointerEventData eventData)
        {
            Appear();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            Fade();
        }
    }
}