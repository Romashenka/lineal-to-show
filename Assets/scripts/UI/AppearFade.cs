﻿using UnityEngine;
using UnityEngine.UI;

namespace BatProject.UI
{
    /// <summary>
    /// Component for UI-elements that should slowly appear and fade.
    /// Appear and fade are methods that should be called from outside.
    /// </summary>
    public class AppearFade : MonoBehaviour
    {
        /// <summary>
        ///Element that should appear and fade.
        /// </summary>
        public Image[] fadingElems;
        /// <summary>
        /// Time till max alpha reached.
        /// Fading time too.
        /// </summary>
        [Range(0f, 1f)]
        public float appearingTime = .5f;
        /// <summary>
        /// Max alpha that should be reached on appear.
        /// </summary>
        [Range(0f, 1f)]
        public float maxAlpha = 1f;

        private bool fadingState;
        private float appearSpeed;
        private float currentAlpha;
        State currentState;

        public virtual void Start()
        {
            if (fadingElems.Length == 0)
                fadingElems = GetComponentsInChildren<Image>();
            if (fadingElems.Length == 0)
            {
                Debug.Log("No fadingElems images found", this);
                Destroy(this);
                return;
            }
            foreach (var el in fadingElems)
            {
                ChangeElemAlpha(el, 0f);
            }

            appearSpeed = maxAlpha / appearingTime;
        }

        void Update()
        {
            switch (currentState)
            {
                case State.appearing:
                    MoveCurrentAlphas(maxAlpha);
                    break;
                case State.fading:
                    MoveCurrentAlphas(0f);
                    break;
                default:
                    break;
            }
        }

        public void Fade()
        {
            currentState = State.fading;
        }

        public void Appear()
        {
            currentState = State.appearing;
        }

        void MoveCurrentAlphas(float target)
        {
            currentAlpha = Mathf.MoveTowards(currentAlpha, target, appearSpeed * Time.deltaTime);

            foreach (var el in fadingElems)
            {
                ChangeElemAlpha(el, currentAlpha);
            }

            if (currentAlpha == target)
            {
                currentState = State.stop;
            }
        }

        void ChangeElemAlpha(Image i, float newAlpha)
        {
            Color c = i.color;
            c.a = newAlpha;
            i.color = c;
        }

        enum State
        {
            fading,
            appearing,
            stop,
        }
    }
}