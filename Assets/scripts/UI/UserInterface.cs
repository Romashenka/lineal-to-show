﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

namespace BatProject.UI
{

    /// <summary>
    /// UI-component that describes UIButtons-methods and logic for showing/hiding some UI-layers.
    /// </summary>
    public class UserInterface : MonoBehaviour
    {
        [Header("Layers")]
        public UILayer pauseMenuLayer;
        public UILayer gameOverMenuLayer;
        public UILayer optionsLayer;

        private Stack<UILayer> openedLayers;

        public bool cleared { get { return openedLayers.Count == 0; } }

        void Awake()
        {
            GlobalEvents.playerDead += OnGameEnded;
            openedLayers = new Stack<UILayer>();
            GlobalEvents.gamePaused += (bool pause) => Cursor.visible = pause;
        }

        void Update()
        {
            if (InputControl.GetButtonDown(Controls.mainButtons.escape))
            {
                if (openedLayers.Count == 0)
                {
                    OpenLayer(pauseMenuLayer);
                }
                else
                {
                    UILayer lastOpened = openedLayers.Pop();
                    lastOpened.SetActive(false);
                }
            }
        }


        public void OpenLayer(UILayer layer)
        {
            if (openedLayers.Count > 0 && layer == openedLayers.Peek())
            {
                return;
            }
            else
            {
                openedLayers.Push(layer);
                layer.SetActive(true);
            }
        }

        public void CloseLayer(UILayer layer)
        {
            if (openedLayers.Peek() == layer)
            {
                openedLayers.Pop().SetActive(false);
            }
        }

        public void ExitToMainMenu()
        {
            SceneManager.LoadScene(0);
        }

        public void RestartLevel()
        {
            GameSaves.SavingSystem.ResetProgressInLevel();
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.UnloadScene(scene);
            SceneManager.LoadScene(scene.name, LoadSceneMode.Single);
            Time.timeScale = 1f;
        }


        /// <summary>
        /// For test only!!!!!!
        /// </summary>
        public void RestartLevelFromCheckpoint()
        {
            Scene current = SceneManager.GetActiveScene();
            SceneManager.LoadScene(current.name);
            Time.timeScale = 1f;
        }

        public void OnGameExit()
        {
            Application.Quit();
        }

        public void OnGameEnded()
        {
            gameOverMenuLayer.gameObject.SetActive(true);
        }

        void OnDisable()
        {
            GlobalEvents.playerDead -= OnGameEnded;
            GlobalEvents.gamePaused -= (bool pause) => Cursor.visible = !pause;
        }
    }
}