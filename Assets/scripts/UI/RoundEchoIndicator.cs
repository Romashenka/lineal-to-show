﻿using UnityEngine.UI;
using UnityEngine;

namespace BatProject.UI
{

    /// <summary>
    /// UI - component, that shows echo-emitter's emit-cycle.
    /// Not finished!!! For test only.
    /// </summary>
    public class RoundEchoIndicator : MonoBehaviour
    {
        public EchoEmitterParams myParams;
        Image image;

        void Start()
        {
            image = GetComponent<Image>();
        }
        void Update()
        {
            image.fillAmount = myParams.phasePercent;
        }
    }
}