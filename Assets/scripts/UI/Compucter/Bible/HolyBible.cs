﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BatProject.Bible
{
    [CreateAssetMenu(menuName = "Test/HolyBible")]
    public class HolyBible : ScriptableObject
    {

        [SerializeField]
        private BibleElement[] abils;
        [SerializeField]
        private BibleElement[] weapons;
        [SerializeField]
        private BibleElement[] chars;
        [SerializeField]
        private BibleElement[] envirnt;

        public BibleElement[] GetElementsList(BibleCategory category)
        {
            switch (category)
            {
                case BibleCategory.Abilityes:
                    return abils;
                case BibleCategory.Characters:
                    return chars;
                case BibleCategory.Environment:
                    return envirnt;
                case BibleCategory.Weapons:
                    return weapons;
                default:
                    Debug.LogError("Unknown category " + category);
                    return null;
            }
        }
    }
}