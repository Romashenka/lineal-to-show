﻿using UnityEngine;
using UnityEngine.UI;
using BatProject.Bible;

namespace BatProject.UI
{
    public class ItemsDescription : MonoBehaviour
    {
        [SerializeField]
        private ScrollRect descriptionScroller;

        [SerializeField]
        private Text header;

        [SerializeField]
        private Text characteristics;

        [SerializeField]
        private Text description;

        [SerializeField]
        private Image image;


        public void ShowDescription(BibleElement element)
        {
            header.text = element.header;
            characteristics.text = "Characteristics:\n" + element.characteristics;
            description.text = element.description;
            image.sprite = element.image;
            descriptionScroller.verticalScrollbar.value = 1f;
        }
    }
}