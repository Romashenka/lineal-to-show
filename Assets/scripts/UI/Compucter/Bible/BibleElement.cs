﻿using UnityEngine;

namespace BatProject.Bible
{
    [System.Serializable]
    public class BibleElement
    {
        public BibleCategory category;
        public string header;
        public string characteristics;
        public string description;
        public Sprite image;
    }
}