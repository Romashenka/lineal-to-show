﻿using UnityEngine;
using UnityEngine.UI;
using BatProject.UI;


namespace BatProject.Bible
{
    public class ElementButton : MonoBehaviour
    {
        [SerializeField]
        private Text buttonText;
        private BibleController controller;
        private BibleElement element;
        public int id { get; private set; }


        public void SetElement(BibleElement element, BibleController controller)
        {
            buttonText.text = element.header;
            this.controller = controller;
            this.element = element;
        }

        public void PresentElement()
        {
            controller.ShowDescription(element);
        }
    }
}