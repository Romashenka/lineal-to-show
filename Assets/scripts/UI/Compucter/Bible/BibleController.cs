﻿using UnityEngine;
using UnityEngine.UI;
using BatProject.Bible;

namespace BatProject.UI
{
    public class BibleController : MonoBehaviour
    {
        [SerializeField]
        private Button firstCategoryButton;
        [SerializeField]
        private ItemsDescription descriptionPanel;
        [SerializeField]
        private HolyBible bible;

        [SerializeField]
        private ButtonsFixer buttonsPanel;

        [SerializeField]
        private ScrollRect buttonsScroller;

        [SerializeField]
        private ElementButton buttonPrefab;

        private ElementButton[] buttons;


        void OnEnable()
        {
            firstCategoryButton.onClick.Invoke();
        }

        public void ShowCategory(int category)
        {
            CleanPrev();
            buttonsScroller.verticalScrollbar.value = 1f;

            BibleElement[] elements = bible.GetElementsList((BibleCategory)category);
            buttons = new ElementButton[elements.Length];
            int length = elements.Length;
            for (int i = 0; i < length; i++)
            {
                buttons[i] = Instantiate(buttonPrefab, buttonsPanel.transform);
                buttons[i].SetElement(elements[i], this);
            }

            buttonsPanel.SetChildrenButtons();

            if (length > 0)
            {
                Button first = buttons[0].GetComponent<Button>();
                first.onClick.Invoke();
            }
        }

        public void ShowDescription(BibleElement element)
        {
            descriptionPanel.ShowDescription(element);
        }

        private void CleanPrev()
        {
            if (buttons != null)
            {
                for (int i = 0; i < buttons.Length; i++)
                {
                    Destroy(buttons[i].gameObject);
                }
                buttons = null;
            }
        }

    }
}