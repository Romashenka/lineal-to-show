﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace BatProject.UI
{
    public class Compucter : UILayer
    {
        public UserInterface mainInterface;
        public UILayer compucterMainPanel;
        public UILayer mapPanel;
        public UILayer missionPanel;
        public UILayer biblePanel;

        private UILayer lastOpened;

        void Start()
        {
            if (compucterMainPanel == null || mapPanel == null || missionPanel == null || biblePanel == null)
            {
                Debug.LogError("Check my references please. I'm destroyed.", this);
                Destroy(gameObject);
                return;
            }

            lastOpened = mapPanel;

            compucterMainPanel.SetActive(false);
            mapPanel.SetActive(false);
            missionPanel.SetActive(false);
            biblePanel.SetActive(false);
        }


        void Update()
        {
            if (InputControl.GetButtonDown(Controls.buttons.map, false))
            {
                OpenClosePanel(mapPanel);
            }

            if (InputControl.GetButtonDown(Controls.buttons.bible, false))
            {
                OpenClosePanel(biblePanel);
            }

            if (InputControl.GetButtonDown(Controls.buttons.missions, false))
            {
                OpenClosePanel(missionPanel);
            }
            if (InputControl.GetButtonDown(Controls.buttons.compucter))
            {
                OpenClosePanel(lastOpened);
            }
        }



        public void OpenClosePanel(UILayer panel)
        {
            if (!compucterMainPanel.gameObject.activeSelf)
            {
                if (!mainInterface.cleared)
                    return;
                mainInterface.OpenLayer(this);
            }

            if (!panel.isActive)
            {
                if (lastOpened != panel)
                {
                    lastOpened.SetActive(false);
                    lastOpened = panel;
                }

                panel.SetActive(true);

                return;
            }
            else
            {
                mainInterface.CloseLayer(this);
                return;
            }
        }


        public override void SetActive(bool active)
        {
            if (!active)
                lastOpened.SetActive(false);
            compucterMainPanel.SetActive(active);
        }
    }
}