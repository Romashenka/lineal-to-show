﻿using UnityEngine;

namespace BatProject.UI
{
    public class Map : MonoBehaviour
    {
        public Shader mapShader;
        public Camera renderCamera;

        void Start()
        {
            renderCamera.gameObject.SetActive(false);
        }

        void OnEnable()
        {
            renderCamera.Render();
            renderCamera.RenderWithShader(mapShader, "");
        }
    }
}