﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace BatProject.UI
{
    /// <summary>
    /// For main menu UI.
    /// Not finished. For testing only.
    /// </summary>
    public class MainMenu____ : MonoBehaviour
    {
        public void NewGame()
        {
            GameSaves.SavingSystem.ResetAllProgress();
            // Debug.Log("Reset");
            SceneManager.LoadScene(1);
            Time.timeScale = 1f;
        }

        public void ContinueGame()
        {
            //REWRITE
            SceneManager.LoadScene(1);
            Time.timeScale = 1f;
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}