﻿using UnityEngine;
using UnityEngine.UI;
using BatProject.Player;
using BatProject.Items;

namespace BatProject.UI
{
    public class PlayerEquipmentUI : MonoBehaviour
    {
        public RectTransform currentEquippedIndicator;
        public WeaponUIWindow[] weaponUIWindows;
        public PlayerEquipment playerEquipment;

        void Start()
        {
            playerEquipment.weaponChanged += OnWeaponChanged;
            playerEquipment.weaponSwitched += SwitchToWeapon;
            if (playerEquipment.equipped != null)
            {
                SetEquipment();
            }
        }

        void OnWeaponChanged(int index, EquipmentWeapon weapon)
        {
            weapon.SetVisual(weaponUIWindows[index]);
            
        }

        void SetEquipment()
        {
            for (int i = 0; i < weaponUIWindows.Length; i++)
            {
                playerEquipment.equipped[i].SetVisual(weaponUIWindows[i]);

            }
            
            currentEquippedIndicator.position = weaponUIWindows[playerEquipment.currentEquippedIndex].transform.position;
        }

        void SwitchToWeapon(int index)
        {
            currentEquippedIndicator.position = weaponUIWindows[index].transform.position;
        }
    }
}