﻿using UnityEngine;
using UnityEngine.UI;
using BatProject.Items;

namespace BatProject.UI
{
    public class WeaponUIWindow : MonoBehaviour, IWeaponUI
    {
        [SerializeField]
        private Image _mainWeaponImage;

        [SerializeField]
        private Image _clipImage;

        [SerializeField]
        private BulletCounter bulletCounter;

        public Sprite mainSprite { set { SetSpriteIfNotNull(_mainWeaponImage, value); } }
        public Sprite clipSprite { set { SetSpriteIfNotNull(_clipImage, value); } }
        public int currentAmmo { set { bulletCounter.ammoCount = value; } }
        public int clipSize { set { bulletCounter.clipSize = value; } }

        public void OnAmmoChanged(int newAmmo)
        {
            bulletCounter.ammoCount = newAmmo;
        }

        public void OnClipChanged(int newCurrentClip)
        {
            bulletCounter.clipCurrent = newCurrentClip;
        }

        void SetSpriteIfNotNull(Image image, Sprite spriteToSet)
        {
            if (spriteToSet == null)
            {
                Color c = image.color;
                c.a = 0f;
                image.color = c;
            }
            else
            {
                Color c = image.color;
                c.a = 1f;
                image.color = c;
                image.sprite = spriteToSet;
            }
        }
    }
}