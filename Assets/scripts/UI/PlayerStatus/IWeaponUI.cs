﻿using UnityEngine;

namespace BatProject.UI
{
    public interface IWeaponUI
    {
        Sprite mainSprite { set; }
        Sprite clipSprite { set; }
        int currentAmmo { set; }
        int clipSize { set; }

        void OnClipChanged(int newAmmo);
        void OnAmmoChanged(int newAmmo);
    }
}