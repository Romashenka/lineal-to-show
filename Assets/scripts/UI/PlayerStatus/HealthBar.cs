﻿using UnityEngine;
using UnityEngine.UI;
using BatProject.Stats;
using BatProject.Player;

namespace BatProject.UI
{

    /// <summary>
    /// UI-component that shows players health.
    /// </summary>
    public class HealthBar : MonoBehaviour
    {
        public PlayerDynamicStats playerDynamicStats;
        public Text text;
        private Slider slider;

        void Start()
        {
            slider = GetComponent<Slider>();
            playerDynamicStats.healthChanged += OnPlayerHealthChanged;
            OnPlayerHealthChanged(playerDynamicStats.health);
        }

        void OnPlayerHealthChanged(float newHealthRate)
        {
            slider.value = newHealthRate;
            text.text = Mathf.FloorToInt(newHealthRate) + " %";
        }

        void OnDestroy()
        {
            playerDynamicStats.healthChanged -= OnPlayerHealthChanged;
        }
    }
}