using UnityEngine;
using System.Collections.ObjectModel;



/// <summary>
/// <see cref="Controls"/> is a set of user defined buttons and axes. It is better to store this file somewhere in your project.
/// </summary>
public static class Controls
{
    /// <summary>
    /// <see cref="Buttons"/> is a set of user defined buttons.
    /// </summary>
    public struct Buttons
    {
        public KeyMapping up;
        public KeyMapping down;
        public KeyMapping left;
        public KeyMapping right;
        public KeyMapping run;
        public KeyMapping showCheckpointTracker;
        public KeyMapping compucter;
        public KeyMapping map;
        public KeyMapping bible;
        public KeyMapping missions;
        public KeyMapping use;
        public KeyMapping fire;
        public KeyMapping secondaryFire;
        public KeyMapping switchWeapon;
        public KeyMapping reload;
        public KeyMapping emitEcho;
    }

    /// <summary>
    /// <see cref="MainButtons"/> is a set of main buttons. Not desirable to be changed by user.
    /// </summary>
    public struct MainButtons
    {
        public KeyMapping escape;
    }

    /// <summary>
    /// <see cref="Axes"/> is a set of user defined axes.
    /// </summary>
    public struct Axes
    {
        public Axis vertical;
        public Axis horizontal;
    }



    /// <summary>
    /// Set of buttons.
    /// </summary>
    public static Buttons buttons;

    /// <summary>
    /// Set of main buttons.
    /// </summary>
    public static MainButtons mainButtons;

    /// <summary>
    /// Set of axes.
    /// </summary>
    public static Axes axes;



    /// <summary>
    /// Initializes the <see cref="Controls"/> class.
    /// </summary>
    static Controls()
    {

        #region  buttons
        buttons.up = InputControl.setKey("Up", KeyCode.W, KeyCode.UpArrow);
        buttons.down = InputControl.setKey("Down", KeyCode.S, KeyCode.DownArrow);
        buttons.left = InputControl.setKey("Left", KeyCode.A, KeyCode.LeftArrow);
        buttons.right = InputControl.setKey("Right", KeyCode.D, KeyCode.RightArrow);
        buttons.run = InputControl.setKey("Run", KeyCode.LeftShift);

        buttons.showCheckpointTracker = InputControl.setKey("Checkpoint Tracker", KeyCode.LeftControl);
        buttons.use = InputControl.setKey("Use", KeyCode.E);

        buttons.map = InputControl.setKey("Map", KeyCode.M);
        buttons.missions = InputControl.setKey("Missions", KeyCode.J);
        buttons.bible = InputControl.setKey("Bible", KeyCode.L);
        buttons.compucter = InputControl.setKey("Computer", KeyCode.Tab);

        buttons.fire = InputControl.setKey("Fire", KeyCode.Mouse0, KeyCode.None);
        buttons.secondaryFire = InputControl.setKey("Aim", KeyCode.Mouse1, KeyCode.None);
        buttons.switchWeapon = InputControl.setKey("Switch weapon", MouseAxis.WheelUp, MouseAxis.WheelDown);
        buttons.reload = InputControl.setKey("Reload weapon", KeyCode.R);

        buttons.emitEcho = InputControl.setKey("Emit echo", KeyCode.Space);
        #endregion


        #region  mainButtons
        mainButtons.escape = InputControl.setKey("escape", KeyCode.Escape);
        #endregion

        #region  axes
        axes.vertical = InputControl.setAxis("Vertical", buttons.down, buttons.up);
        axes.horizontal = InputControl.setAxis("Horizontal", buttons.left, buttons.right);
        #endregion
        load(false);
    }

    /// <summary>
    /// Nothing. It just call static constructor if needed.
    /// </summary>
    public static void init()
    {
        // Nothing. It just call static constructor if needed
    }

    public static void loadDefaults() { load(true); }

    /// <summary>
    /// Save controls.
    /// </summary>
    public static void save() { save(false); }

    private static void save(bool defaults)
    {
        ReadOnlyCollection<KeyMapping> keys = InputControl.getKeysList();
        string prefix = defaults ? "DefaultControls." : "Controls.";
        foreach (KeyMapping key in keys)
        {
            PlayerPrefs.SetString(prefix + key.name + ".primary", key.primaryInput.ToString());
            PlayerPrefs.SetString(prefix + key.name + ".secondary", key.secondaryInput.ToString());
            PlayerPrefs.SetString(prefix + key.name + ".third", key.thirdInput.ToString());
        }

        PlayerPrefs.Save();
    }

    /// <summary>
    /// Load controls.
    /// </summary>
    private static void load(bool defaults)
    {
        ReadOnlyCollection<KeyMapping> keys = InputControl.getKeysList();

        if (!defaults && PlayerPrefs.GetString("DefaultControls." + keys[0] + ".primary") == null)
        {
            save(true);
            Debug.Log("saved");
        }

        string prefix = defaults ? "DefaultControls." : "Controls.";

        foreach (KeyMapping key in keys)
        {
            string inputStr;

            inputStr = PlayerPrefs.GetString(prefix + key.name + ".primary");

            if (inputStr != "")
            {
                key.primaryInput = customInputFromString(inputStr);
            }

            inputStr = PlayerPrefs.GetString(prefix + key.name + ".secondary");

            if (inputStr != "")
            {
                key.secondaryInput = customInputFromString(inputStr);
            }

            inputStr = PlayerPrefs.GetString(prefix + key.name + ".third");

            if (inputStr != "")
            {
                key.thirdInput = customInputFromString(inputStr);
            }
        }
    }

    /// <summary>
    /// Converts string representation of CustomInput to CustomInput.
    /// </summary>
    /// <returns>CustomInput from string.</returns>
    /// <param name="value">String representation of CustomInput.</param>
    private static CustomInput customInputFromString(string value)
    {
        CustomInput res;

        res = JoystickInput.FromString(value);

        if (res != null)
        {
            return res;
        }

        res = MouseInput.FromString(value);

        if (res != null)
        {
            return res;
        }

        res = KeyboardInput.FromString(value);

        if (res != null)
        {
            return res;
        }

        return null;
    }
}

