﻿using UnityEngine;

namespace BatProject
{
    ///<summary>
    ///<see cref="AreaAbility"/> is a base class for ability components, that have area effects.
    ///</summary>
    public class AreaAbility : Ability
    {
        ///<summary>
        ///Lifetime of AreaAbility.
        ///</summary>
        public float time;

        void Start()
        {
            Destroy(gameObject, time);
        }

        void Update()
        {
            transform.position = transform.parent.position;
        }

        ///<summary>
        ///Instantiates this prefab in origin position and rotation.
        ///</summary>
        ///<param name="origin">Place where Ability will be instantiated</param>
        public override void Use(Transform origin)
        {
            Instantiate(this, origin.position, origin.rotation, origin);
        }
    }
}