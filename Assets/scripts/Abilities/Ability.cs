﻿using UnityEngine;

namespace BatProject
{
    ///<summary>
    ///<see cref="Ability"/> is a base abstract for all ability components.
    ///</summary> 
    public abstract class Ability : MonoBehaviour
    {

        ///<summary>
        ///Use ability.
        ///</summary> 
        /// <param name="origin">origin that uses ability. </param>
        public abstract void Use(Transform origin);
    }
}