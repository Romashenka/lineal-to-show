﻿using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Ability component that behaves like a projectile.
    ///</summary>
    public class ProjectileAbility : Ability
    {

        public float lifeTime;

        public float speed;

        /// <summary>
        /// Effects, that will be instantiated on target after collision.
        ///</summary>
        public Effects.Effect[] effects;

        /// <summary>
        /// Visual or other effect prefab that will be instantiated after collision with something.
        ///</summary>
        public GameObject OnDestroyEffect;

        private Rigidbody2D myRb { get; set; }


        void Awake()
        {
            myRb = GetComponent<Rigidbody2D>();
            Destroy(gameObject, lifeTime);
        }

        /// <summary>
        /// Launch projectile.
        ///</summary>
        ///<param name="origin">Original position of projectile. Direction will be relative to origin.up vector.</param>
        public override void Use(Transform origin)
        {
            Vector2 direction = origin.TransformDirection(Vector2.up);
            Use(origin, direction);
        }

        /// <summary>
        /// Launch projectile.
        ///</summary>
        ///<param name="origin">Original position of projectile.</param>
        ///<param name="direction">Direction of projectile not relative to rotation of origin.</param>
        public void Use(Transform origin, Vector2 direction)
        {
            ProjectileAbility ability = Instantiate(this, origin.position, Quaternion.identity);
            ability.myRb.AddForce(direction * speed, ForceMode2D.Impulse);
        }

        protected virtual void OnCollisionEnter2D(Collision2D coll)
        {
            Unit unit = coll.gameObject.GetComponent<Unit>();
            if (unit != null)
            {
                foreach (var e in effects)
                {
                    e.Begin(unit);
                }
            }
            Destroy(gameObject);
        }

        //Add animation instead
        void OnDisable()
        {
            Instantiate(OnDestroyEffect, transform.position, transform.rotation);
        }
    }
}