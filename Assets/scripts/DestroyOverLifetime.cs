﻿using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Component, that destroy gameObject over some time.
    /// </summary>
    public class DestroyOverLifetime : MonoBehaviour
    {
        public float lifeTime = 1f;

        void Start()
        {
            Destroy(gameObject, lifeTime);
        }
    }
}