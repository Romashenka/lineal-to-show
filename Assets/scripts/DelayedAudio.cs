﻿using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Component, that delays playing of play-on-awake <see cref="AudioSource"/> for random time.
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class DelayedAudio : MonoBehaviour
    {
        /// <summary>
        /// Max in random from - 0 to - Max.
        /// </summary>
        public float maxDelayTime = 3f;

        void Start()
        {
            AudioSource source = GetComponent<AudioSource>();
            float delayTime = Random.Range(0f, maxDelayTime);
            source.PlayDelayed(delayTime);
            Destroy(this);
        }
    }
}