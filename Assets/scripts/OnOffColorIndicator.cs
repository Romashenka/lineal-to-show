﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Component, that can change color back and forth .
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public class OnOffColorIndicator : MonoBehaviour, IOnOff
    {
        SpriteRenderer spriteRenderer;

        void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void SetOnOff(bool active)
        {
            if (active)
            {
                spriteRenderer.color = Color.red;
            }
            else
            {
                spriteRenderer.color = Color.white;
            }
        }
    }
}