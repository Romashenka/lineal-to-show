﻿using UnityEngine;

namespace BatProject.Util
{
    /// <summary>
    /// Quaternion that can be serialized.
    /// </summary>
    [System.Serializable]
    public class QuaternionSerializable
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public QuaternionSerializable(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public static implicit operator QuaternionSerializable(Quaternion q)
        {
            return new QuaternionSerializable(q.x, q.y, q.z, q.w);
        }

        public static explicit operator Quaternion(QuaternionSerializable serializable)
        {
            return new Quaternion(serializable.x, serializable.y, serializable.z, serializable.w);
        }
    }
}