﻿using UnityEngine;

namespace BatProject.Util
{
    /// <summary>
    /// Vector2 that can be serialized.
    /// </summary>
    [System.Serializable]
    public class Vector2Serializable
    {
        public float x;
        public float y;

        public Vector2Serializable(float x = 0f, float y = 0f)
        {
            this.x = x;
            this.y = y;
        }

        public static implicit operator Vector2Serializable(Vector2 v)
        {
            return new Vector2Serializable(v.x, v.y);
        }

        public static explicit operator Vector2(Vector2Serializable serializable)
        {
            return new Vector2(serializable.x, serializable.y);
        }
    }
}