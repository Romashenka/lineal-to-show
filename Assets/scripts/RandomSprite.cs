﻿using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Component, that changes <see cref="SpriteRenderer"/> sprite to random sprite from the array.
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public class RandomSprite : MonoBehaviour
    {
        public Sprite[] sprites;

        void Start()
        {
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            int rnd = Random.Range(0, sprites.Length);
            sr.sprite = sprites[rnd];
        }
    }
}