﻿using System;
using UnityEngine;

namespace BatProject.Receptors
{
    /// <summary>
    /// Component, that provides callbacks if something enters it's collider.
    /// Sees only in defined frustrum and doesn't see throw obstacles.
    /// </summary>
    public class FieldOfViewObsCheck : MonoBehaviour
    {
        /// <summary>
        /// Will not see throw this layer.
        /// </summary>
        public LayerMask obstaclesLayer;

        /// <summary>
        /// Time between slow operations of checking the obstacles are between center and target.
        /// And between operations of checking if target in frustrum.
        /// </summary>
        public float obstCheckPeriod = .5f;

        /// <summary>
        /// Angle of view.
        /// </summary>
        public float frustrumAngle = 45f;

        public Action<Collider2D> enter;
        public Action<Collider2D> stay;
        public Action<Collider2D, ExitMode> exit;

        bool mentioned;
        bool visible;
        bool behindObstacles = true;
        bool outOfFrustrum = true;
        float time;

        protected void OnTriggerStay2D(Collider2D coll)
        {
            if (Time.time > time + obstCheckPeriod)
            {
                outOfFrustrum = !CheckFrustrum(coll.transform.position);
                if (!outOfFrustrum)
                    behindObstacles = CheckObstacles(coll.transform.position);
                time = Time.time;
            }

            if (mentioned)
            {
                if (outOfFrustrum)
                {
                    OnExit(coll, ExitMode.byFrustrum);
                }
                else if (behindObstacles)
                {
                    OnExit(coll, ExitMode.byObstacle);
                }
                else
                {
                    OnStay(coll);
                }
            }
            else
            {
                if (outOfFrustrum || behindObstacles)
                    return;
                else
                {
                    OnEnter(coll);
                }
            }
        }

        protected void OnTriggerExit2D(Collider2D coll)
        {
            if (mentioned)
            {
                OnExit(coll, ExitMode.byRadius);
            }
        }

        protected void OnEnter(Collider2D coll)
        {
            mentioned = true;
            if (enter != null)
                enter(coll);
        }

        protected void OnStay(Collider2D coll)
        {
            if (stay != null)
                stay(coll);
        }

        protected void OnExit(Collider2D coll, ExitMode mode)
        {
            mentioned = false;
            if (exit != null)
                exit(coll, mode);
        }

        public bool CheckIfInFOV(Vector2 targetPos)
        {
            return (!CheckObstacles(targetPos) && CheckFrustrum(targetPos));
        }


        /// <summary>
        /// Checks if there ARE obstacles between.
        /// </summary>
        bool CheckObstacles(Vector2 targetPos)
        {
            float dist = Vector2.Distance(targetPos, transform.position);
            var rcH = Physics2D.Raycast(transform.position, (Vector3)targetPos - transform.position, dist, obstaclesLayer);
            if (rcH.collider != null)
                return true;
            else
                return false;
        }


        /// <summary>
        /// Checks if target IN frustrum.
        /// </summary>
        bool CheckFrustrum(Vector2 targetPos)
        {
            float angle = Vector2.Angle(transform.TransformDirection(Vector2.up), targetPos - (Vector2)transform.position);

            if (angle < frustrumAngle / 2f)
                return true;
            else
                return false;
        }

        void OnDrawGizmos()
        {
            float l = Mathf.Deg2Rad * (90f + frustrumAngle / 2f);
            float r = Mathf.Deg2Rad * (90f - frustrumAngle / 2f);

            Vector3 lv = transform.TransformDirection(new Vector3(Mathf.Cos(l), Mathf.Sin(l)) * 5f);
            Vector3 rv = transform.TransformDirection(new Vector3(Mathf.Cos(r), Mathf.Sin(r)) * 5f);
            if (mentioned)
                Gizmos.color = Color.red;
            else
                Gizmos.color = Color.blue;
            Gizmos.DrawRay(transform.position, lv);
            Gizmos.DrawRay(transform.position, rv);
        }

        /// <summary>
        /// Returning in OnExit callback contains information how target gone.
        /// </summary>
        public enum ExitMode
        {
            byObstacle,
            byFrustrum,
            byRadius,
        }
    }
}
