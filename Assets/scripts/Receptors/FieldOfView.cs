﻿using UnityEngine;
using System;

namespace BatProject.Receptors
{
    /// <summary>
    /// Component, that provides callbacks if something enters it's collider.
    ///</summary>
    [RequireComponent(typeof(Collider2D))]
    public class FieldOfView : Receptor
    {
        protected void OnTriggerEnter2D(Collider2D coll)
        {
            OnEnter(coll);
        }

        protected void OnTriggerStay2D(Collider2D coll)
        {
            OnStay(coll);
        }

        protected void OnTriggerExit2D(Collider2D coll)
        {
            OnExit(coll);
        }

        void OnDrawGizmos()
        {
            float radis = GetComponent<CircleCollider2D>().radius;
            Gizmos.color = Color.gray;
            Gizmos.DrawWireSphere(transform.position, radis);
        }
    }
}