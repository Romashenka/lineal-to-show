﻿using System;
using UnityEngine;

namespace BatProject.Receptors
{

    /// <summary>
    /// Component, that provides callbacks if something enters it's collider.
    /// </summary>
    public abstract class Receptor : MonoBehaviour
    {

        public Action<Collider2D> enter;
        public Action<Collider2D> stay;
        public Action<Collider2D> exit;


        protected virtual void OnEnter(Collider2D coll)
        {
            if (enter != null)
                enter(coll);
        }


        protected virtual void OnStay(Collider2D coll)
        {
            if (stay != null)
                stay(coll);
        }

        protected virtual void OnExit(Collider2D coll)
        {
            if (exit != null)
                exit(coll);
        }
    }
}