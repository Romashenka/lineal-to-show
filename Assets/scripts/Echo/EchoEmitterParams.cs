﻿using UnityEngine;


/// <summary>
/// Data-containing scriptable object for echo-emitting system.
///</summary>
[CreateAssetMenu]
public class EchoEmitterParams : ScriptableObject
{
    [SerializeField]
    private float _emitPeriod = 2.4f;
    /// <summary>
    /// Time between two emission-cycles.
    ///</summary>
    public float emitPeriod { get { return _emitPeriod; } }


    [SerializeField]
    private float _speed = 40f;
    /// <summary>
    /// Speed of echo-wave.
    ///</summary>    
    public float speed { get { return _speed; } }

    /// <summary>
    /// Max radius echo-wave goes.
    ///</summary>
    public float maxRadius { get { return (_emitPeriod - refreshPeriodPercent * emitPeriod) * speed; } }

    [SerializeField]
    private float _refreshPeriodPercent = 0f;
    /// <summary>
    /// Delay between the end of one cycle and the beginning of another.
    ///</summary>
    public float refreshPeriodPercent { get { return _refreshPeriodPercent; } }


    /// <summary>
    /// Percent of cycle in certain time. Is assigning by <see cref="EchoMotor"/>
    ///</summary>
    public float phasePercent { get; set; }

}
