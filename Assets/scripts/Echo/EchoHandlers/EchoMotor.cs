﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace BatProject.Echo
{
    /// <summary>
    /// Component, that defines logic for echo-cycle.
    /// </summary>
    public class EchoMotor : MonoBehaviour
    {
        public EchoEmitterParams echoParams;
        /// <summary>
        /// Is emission auto-triggered or called from outside.
        /// </summary>
        public bool loopEmission;
        public Action<Vector2> echoCycleStarted;
        public Action<float> radiusChanged;
        public Action echoRadiusMaxed;

        public float EchoRadius
        {
            get
            {
                return (Time.time - lastEmitionTime) * echoParams.speed;
            }
        }

        private float lastEmitionTime;
        private bool waiting;

        // void Start()
        // {
        //     GlobalEvents.gameLoaded += Emit;
        // }

        void Update()
        {
            if (!loopEmission && InputControl.GetButtonDown(Controls.buttons.emitEcho))
            {
                ForceEmition();
            }

            echoParams.phasePercent = GetPhasePercent();
            if (!waiting)
            {
                if (Time.time - lastEmitionTime > echoParams.emitPeriod)
                {
                    waiting = true;
                    if (echoRadiusMaxed != null)
                        echoRadiusMaxed();
                    if (loopEmission)
                        Emit();
                }
                else
                {
                    radiusChanged(EchoRadius);
                }
            }
        }

        public void ForceEmition()
        {
            if (CheckEmitValid())
            {
                Emit();
            }
        }
        bool CheckEmitValid()
        {
            if (Time.time > lastEmitionTime + echoParams.emitPeriod)
                return true;
            else
                return false;
        }

        void Emit()
        {
            waiting = false;
            lastEmitionTime = Time.time;

            if (echoCycleStarted != null)
            {
                echoCycleStarted(transform.position);
            }
        }

        float GetPhasePercent()
        {
            if (!waiting)
                return (Time.time - lastEmitionTime) / echoParams.emitPeriod;
            else
                return 1f;
        }

        void OnDestroy()
        {
            GlobalEvents.gameLoaded -= Emit;
        }
    }
}