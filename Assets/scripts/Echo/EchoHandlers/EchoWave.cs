﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BatProject.Echo
{
    [RequireComponent(typeof(CircleCollider2D))]
    public class EchoWave : MonoBehaviour
    {

        private CircleCollider2D myColl;

        public System.Action<Vector2> triggetEntered2D;

        void Awake()
        {
            myColl = GetComponent<CircleCollider2D>();
        }

        public void ChangeRadius(float radius)
        {
            myColl.radius = radius;
        }

        void OnTriggerEnter2D(Collider2D coll)
        {
            if (triggetEntered2D != null)
            {
                triggetEntered2D(coll.transform.position);
            }
            //coll.gameObject.GetComponent<SparkleOnCollision> ().Sparkle ();
        }
    }
}