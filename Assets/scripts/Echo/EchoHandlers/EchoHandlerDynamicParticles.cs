﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BatProject.Echo
{
    public class EchoHandlerDynamicParticles : EchoHandlerByCollider
    {
        public ParticleSystem indicatorPrefab;

        public float indicatorLifetime = 1.5f;

        protected override void SetLocatedObjectIndicator(Vector2 position)
        {
            Instantiate(indicatorPrefab, position, Quaternion.identity);
        }
    }
}