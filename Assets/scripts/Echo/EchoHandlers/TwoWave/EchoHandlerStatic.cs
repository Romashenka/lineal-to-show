﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace BatProject.Echo
{
    /// <summary>
    /// Component, that uses material with concrete shader to vizualize echo.
    /// Can create two echo-waves that can overlap each other.
    ///</summary>
    [RequireComponent(typeof(EchoMotor))]
    public class EchoHandlerStatic : MonoBehaviour
    {
        /// <summary>
        /// With some of Echolocation-Two-Wave shaders.
        ///</summary>
        public Material staticObjectsEchoMat;

        /// <summary>
        /// In what rate of echo-cycle material will be completely unlighted.
        ///</summary>

        [Range(0f, 2f)]
        public float fadeRateToPeriod = 1f;

        private EchoMotor echoMotor;


        // All the next fields are used to flip waves between cycles.
        private bool evenWave = true;
        private string matCenterProp = "_Center1";
        private string matRadiusProp = "_Radius1";
        private string matAlphaProp = "_EchoAlpha1";

        private float aplhaDecreaseSpeed { get { return 1f / (echoMotor.echoParams.emitPeriod * fadeRateToPeriod); } }
        void Start()
        {
            echoMotor = GetComponent<EchoMotor>();
            echoMotor.echoCycleStarted += OnCycleStarted;
            echoMotor.radiusChanged += OnRadiusChanged;
        }

        /// <summary>
        /// Decrease alphas of both waves.
        ///</summary>
        void Update()
        {

            float alpha1 = staticObjectsEchoMat.GetFloat("_EchoAlpha1");
            float alpha2 = staticObjectsEchoMat.GetFloat("_EchoAlpha2");

            staticObjectsEchoMat.SetFloat("_EchoAlpha1", alpha1 - aplhaDecreaseSpeed * Time.deltaTime);
            staticObjectsEchoMat.SetFloat("_EchoAlpha2", alpha2 - aplhaDecreaseSpeed * Time.deltaTime);
        }

        void OnCycleStarted(Vector2 center)
        {
            ChangeMatPropNames();
            staticObjectsEchoMat.SetVector(matCenterProp, center);
            SetWaveAlpha(1f);
        }

        void OnRadiusChanged(float radius)
        {
            staticObjectsEchoMat.SetFloat(matRadiusProp, radius);
        }

        /// <summary>
        /// Switch between waves and operate the same way.
        ///</summary>
        void ChangeMatPropNames()
        {
            if (evenWave)
            {
                matCenterProp = "_Center2";
                matRadiusProp = "_Radius2";
                matAlphaProp = "_EchoAlpha2";
            }
            else
            {
                matCenterProp = "_Center1";
                matRadiusProp = "_Radius1";
                matAlphaProp = "_EchoAlpha1";
            }
            evenWave = !evenWave;
        }

        void SetWaveAlpha(float a)
        {
            staticObjectsEchoMat.SetFloat(matAlphaProp, a);
        }

        void OnDisable()
        {
            SetWaveAlpha(0f);
            staticObjectsEchoMat.SetFloat("_Radius1", 0f);
            staticObjectsEchoMat.SetFloat("_Radius2", 0f);
        }
    }
}