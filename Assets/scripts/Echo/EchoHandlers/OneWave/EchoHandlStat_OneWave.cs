﻿using UnityEngine;

namespace BatProject.Echo
{
    /// <summary>
    /// Component, that uses material with concrete shader to vizualize echo.
    ///</summary>
    [RequireComponent(typeof(EchoMotor))]
    public class EchoHandlStat_OneWave : MonoBehaviour
    {
        /// <summary>
        /// Material with some of Echolocation-One-Wave shaders.
        ///</summary>
        public Material staticObjectsEchoMat;

        /// <summary>
        /// In what rate of echo-cycle material will be completely unlighted.
        ///</summary>
        [Range(0f, 2f)]
        public float fadeRateToPeriod = 1f;
        private EchoMotor echoMotor;
        private float initialAlpha;

        private int echoColorPropID;
        private int radiusPropID;
        private int centerPropID;

        void Start()
        {
            echoColorPropID = Shader.PropertyToID("_EchoColor");
            radiusPropID = Shader.PropertyToID("_Radius");
            centerPropID = Shader.PropertyToID("_Center");

            initialAlpha = staticObjectsEchoMat.GetColor(echoColorPropID).a;


            echoMotor = GetComponent<EchoMotor>();
            echoMotor.echoCycleStarted += OnCycleStarted;
            echoMotor.radiusChanged += OnRadiusChanged;
        }

        void OnCycleStarted(Vector2 center)
        {
            SetWaveCenter(center);
            SetWaveAlpha(initialAlpha);
            staticObjectsEchoMat.SetFloat(radiusPropID, echoMotor.EchoRadius);
        }

        protected virtual void OnRadiusChanged(float radius)
        {
            staticObjectsEchoMat.SetFloat(radiusPropID, radius);
            SetWaveAlpha((fadeRateToPeriod - radius / echoMotor.echoParams.maxRadius) * initialAlpha);
        }

        void SetWaveCenter(Vector2 center)
        {
            Vector4 cent = staticObjectsEchoMat.GetVector(centerPropID);
            cent.x = center.x;
            cent.y = center.y;

            cent.w = transform.eulerAngles.z * Mathf.Deg2Rad;
            staticObjectsEchoMat.SetVector(centerPropID, cent);
        }

        void SetWaveAlpha(float a)
        {
            Color c = staticObjectsEchoMat.GetColor(echoColorPropID);
            c.a = a;
            staticObjectsEchoMat.SetColor(echoColorPropID, c);
        }
        void OnDisable()
        {
            SetWaveAlpha(initialAlpha);
            staticObjectsEchoMat.SetFloat(radiusPropID, 0f);
        }
    }
}