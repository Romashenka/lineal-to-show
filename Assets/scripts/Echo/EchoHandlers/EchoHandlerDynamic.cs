﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BatProject.Echo

{
    /// <summary>
    /// Component for echo, that visualize objects by instantiation of marks by contact. Useful for visualization only of the position
    /// of target-objects in moment of contact.
    ///</summary>
    public class EchoHandlerDynamic : EchoHandlerByCollider
    {
        /// <summary>
        /// Mark of located object.
        ///</summary>
        public GameObject locatedObjectPrefab;
        /// <summary>
        /// Material of marks.
        ///</summary>
        public Material locatedObjectMaterial;
        /// <summary>
        /// To destroy all on echo-cycle ended.
        ///</summary>
        private List<GameObject> locatedObjMarks;

        override protected void Start()
        {
            base.Start();
            locatedObjMarks = new List<GameObject>();
        }

        void Update()
        {
            locatedObjectMaterial.SetVector("_PlayerPosition", transform.position);
        }

        override protected void OnEchoRadiusMaxed()
        {
            foreach (var obj in locatedObjMarks)
            {
                Destroy(obj);
            }
            locatedObjMarks = new List<GameObject>(locatedObjMarks.Count);
        }

        override protected void SetLocatedObjectIndicator(Vector2 position)
        {
            locatedObjMarks.Add(Instantiate(locatedObjectPrefab, position, Quaternion.identity));
        }
    }
}