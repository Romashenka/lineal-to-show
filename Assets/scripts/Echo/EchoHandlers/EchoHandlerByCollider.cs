﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BatProject.Echo
{
    /// <summary>
    /// Abstract class for EchoHandler-components that acts by instantiating some wave-gameobject and increasing radius of it.
    ///</summary>
    public abstract class EchoHandlerByCollider : MonoBehaviour
    {
        public EchoMotor motor;

        public EchoWave wavePrefab;

        private EchoWave waveInstance;

        protected virtual void Start()
        {
            motor.echoCycleStarted += OnEchoCycleStarted;
            motor.radiusChanged += OnRadiusChanged;
            motor.echoRadiusMaxed += OnEchoRadiusMaxed;
        }

        protected void OnEchoCycleStarted(Vector2 center)
        {
            if (waveInstance == null)
            {
                waveInstance = Instantiate(wavePrefab, center, Quaternion.identity);
                waveInstance.triggetEntered2D += SetLocatedObjectIndicator;
            }
            else
            {
                waveInstance.transform.position = transform.position;
                waveInstance.gameObject.SetActive(true);
            }
        }

        protected virtual void OnEchoRadiusMaxed()
        {
            waveInstance.gameObject.SetActive(false);
        }

        /// <summary>
        /// Set indicator to object that toched by wave.
        ///</summary>
        protected abstract void SetLocatedObjectIndicator(Vector2 posotion);

        void OnRadiusChanged(float radius)
        {
            waveInstance.ChangeRadius(radius);
        }
    }
}