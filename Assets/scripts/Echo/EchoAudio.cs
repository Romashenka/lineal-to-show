﻿using UnityEngine;

namespace BatProject.Echo
{
    /// <summary>
    /// Audio that plays on echo-cycle started.
    ///</summary>
    [RequireComponent(typeof(EchoMotor), typeof(AudioSource))]
    public class EchoAudio : MonoBehaviour
    {
        private EchoMotor motor;
        private AudioSource audioSource;

        void Start()
        {
            motor = GetComponent<EchoMotor>();
            audioSource = GetComponent<AudioSource>();
            motor.echoCycleStarted += OnEchoCycleStarted;
        }

        void OnEchoCycleStarted(Vector2 v)
        {
            audioSource.Play();
        }
    }
}