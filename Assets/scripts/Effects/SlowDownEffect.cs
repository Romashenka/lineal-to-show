﻿using System.Collections;
using UnityEngine;

namespace BatProject.Effects
{
    /// <summary>
    /// Scriptable object effect, that applyes decreasing of speed rate of unit.
    ///</summary>
    [CreateAssetMenu(menuName = "Effects/SlowDown")]
    public class SlowDownEffect : Effect
    {

        /// <summary>
        /// Speed rate that have unit after applying.
        ///</summary>
        [Range(0f, 1f)]
        public float decreasePercent;
        /// <summary>
        /// Total time of slow down.
        ///</summary>
        public float time;

        public override void Begin(Unit target)
        {
            target.StartCoroutine(MultiplySpeedRate(target));
        }

        IEnumerator MultiplySpeedRate(Unit target)
        {
            target.statsDynamic.speedRate *= decreasePercent;
            yield return new WaitForSeconds(time);
            End(target);
        }

        public override void End(Unit target)
        {
            target.statsDynamic.speedRate /= decreasePercent;
        }
    }
}