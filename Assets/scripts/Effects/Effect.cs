﻿using UnityEngine;

namespace BatProject.Effects
{
    /// <summary>
    /// Abstract scriptable-object class for effecting on unit.
    ///</summary>
    public abstract class Effect : ScriptableObject
    {
        public abstract void Begin(Unit target);

        public abstract void End(Unit target);
    }
}