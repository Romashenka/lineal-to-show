﻿using System.Collections.Generic;
using UnityEngine;

namespace BatProject.Effects
{
    /// <summary>
    /// Components, that contains effects that must be applyed on some field.
    ///</summary>
    [RequireComponent(typeof(Collider2D))]
    public class EffectField : MonoBehaviour
    {
        /// <summary>
        /// Effects, that will be applied to field of collider.
        ///</summary>
        public Effect[] effects;

        /// <summary>
        /// Units that entered the field. To take away applied effects if field destroyed.
        ///</summary>
        private List<Unit> entered;

        void Start()
        {
            entered = new List<Unit>();
        }

        void OnTriggerEnter2D(Collider2D coll)
        {
            Unit unit = coll.GetComponent<Unit>();
            if (unit == null)
                return;
            foreach (Effect e in effects)
            {
                e.Begin(unit);
            }
            entered.Add(unit);
        }

        void OnTriggerExit2D(Collider2D coll)
        {
            Unit unit = coll.GetComponent<Unit>();
            if (unit == null)
                return;
            foreach (Effect e in effects)
            {
                e.End(unit);
            }
            entered.Remove(unit);
        }

        void OnDestroy()
        {
            foreach (var unit in entered)
            {
                foreach (var eff in effects)
                {
                    eff.End(unit);
                }
            }
        }
    }
}