﻿using System.Collections;
using UnityEngine;

namespace BatProject.Effects
{
    /// <summary>
    /// Scriptable object effect, that describes damage that applyes over certain periods of time.
    ///</summary>
    [CreateAssetMenu(menuName = "Effects/DamageOverTime")]
    public class DamageOverTimeEffect : Effect
    {
        /// <summary>
        /// Damage that applyes over and over.
        ///</summary>
        public float damage;
        /// <summary>
        /// Time between applying damage cycles.
        ///</summary>
        public float period;
        /// <summary>
        /// Total time this effect lasts.
        ///</summary>
        public float time;

        public override void Begin(Unit target)
        {
            int duration = Mathf.FloorToInt(time / period);
            target.StartCoroutine(DOT(target, duration));
        }

        IEnumerator DOT(Unit target, int duration)
        {
            for (int i = 0; i < duration; i++)
            {
                target.GetDamage(damage);
                yield return new WaitForSeconds(period);
            }
        }

        public override void End(Unit target) { }
    }
}