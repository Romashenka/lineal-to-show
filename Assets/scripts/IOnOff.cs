﻿
namespace BatProject
{
    /// <summary>
    /// Interface of Switch On - Switch Off behaviour.
    /// </summary>
    public interface IOnOff
    {
        /// <summary>
        /// Active==true - switch On.
        /// </summary>
        void SetOnOff(bool active);
    }
}