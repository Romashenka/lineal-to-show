﻿using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Component, that makes gameObject smoothly follow the target.
    /// </summary>
    public class CameraFollow : MonoBehaviour
    {
        /// <summary>
        /// What camera is following.
        /// </summary>
        public Transform target;
        /// <summary>
        /// How fast camera starts/stops moving.
        /// </summary>
        public float smoothing;
        /// <summary>
        /// Should camera follow rotation of target.
        /// </summary>
        public bool followRotation;

        [Header("Constrains")]
        /// <summary>
        /// Disable down-move of camera.
        /// </summary>
        public bool disableDown = false;
        /// <summary>
        /// Disable up-move of camera.
        /// </summary>
        public bool disableUp = false;
        /// <summary>
        /// Disable right-move of camera.
        /// </summary>
        public bool disableRight = false;
        /// <summary>
        /// Disable left-move of camera.
        /// </summary>
        public bool disableLeft = false;

        /// <summary>
        /// Distance between camera and target on start
        /// </summary>
        [SerializeField]
        private Vector3 offsetPosition;
        /// <summary>
        /// Difference in rotation of camera and target on start
        /// </summary>
        [SerializeField]
        private Vector3 offsetRotation;

        void Update()
        {
            if (target == null)
                return;
            Vector3 targetCamPos = target.position + offsetPosition;

            if ((disableDown && transform.position.y > targetCamPos.y) || (disableUp && transform.position.y < targetCamPos.y))
            {
                targetCamPos.y = transform.position.y;
            }
            if ((disableLeft && transform.position.x > targetCamPos.x) || (disableRight && transform.position.x < targetCamPos.x))
            {
                targetCamPos.x = transform.position.x;
            }
            transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
            if (followRotation)
            {
                Quaternion targetCamRot = Quaternion.Euler(target.rotation.eulerAngles + offsetRotation);
                transform.rotation = Quaternion.Lerp(transform.rotation, targetCamRot, smoothing * Time.deltaTime);
            }
        }

        void OnDrawGizmosSelected()
        {
            if (target != null)
            {
                Gizmos.DrawSphere(target.position, .5f);
                offsetPosition = transform.position - target.position;
                offsetRotation = transform.rotation.eulerAngles - target.rotation.eulerAngles;
            }
        }
    }
}