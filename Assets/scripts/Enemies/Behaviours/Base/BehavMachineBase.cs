﻿using UnityEngine;

namespace BatProject.BehavioursSystem
{
    /// <summary>
    /// Base class for concrete behaviourMachines.
    ///</summary>
    public class BehavMachineBase
    {
        /// <summary>
        /// Currently acting behaviour.
        ///</summary>
        private IBehavBase _currentBehav;
        /// <summary>
        /// Since the last behaviour-change.
        ///</summary>
        private float time;

        protected IBehavBase currentBehaviour
        {
            get { return _currentBehav; }
            set
            {
                if (value != _currentBehav)
                {
                    if (_currentBehav != null)
                        _currentBehav.Exit();
                    _currentBehav = value;
                    _currentBehav.Enter();
                    time = Time.time;
                }
            }
        }

        public void Update()
        {
            _currentBehav.Update();
        }

        public void FixedUpdate()
        {
            _currentBehav.FixedUpdate();
        }

        public bool CheckBehaviourTimeElapsed(float time)
        {
            if (Time.time > this.time + time)
                return true;
            return false;
        }
    }
}
