﻿using UnityEngine;

namespace BatProject.BehavioursSystem
{
    /// <summary>
    /// Common interface for behaviours.
    ///</summary>
    public interface IBehavBase
    {
        /// <summary>
        /// On enter the behaviour.
        ///</summary>
        void Enter();
        /// <summary>
        /// On exit behaviour.
        ///</summary>
        void Exit();

        /// <summary>
        /// For updating frame-rate connected acts of behaviour.
        ///</summary>
        void Update();
        /// <summary>
        /// For updating not frame-rate connected acts of behaviour.
        ///</summary>
        void FixedUpdate();
    }
}