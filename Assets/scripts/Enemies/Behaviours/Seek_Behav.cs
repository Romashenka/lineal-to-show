﻿using Pathfinding;
using UnityEngine;

namespace BatProject.BehavioursSystem
{
    /// <summary>
    /// Behaviour that uses pathfinding to walk around without purpouse.
    ///</summary>
    public class Seek_Behav : IBehavBase
    {
        private ISeekBehaviourHandler behavrst;
        private System.Action currentAction;

        /// <summary>
        /// Used by path-seeker component.
        ///</summary>
        private Path currentPath;

        /// <summary>
        /// On current path, ok.
        ///</summary>
        private int currentWaypoint = 0;

        /// <summary>
        /// Distance from position of behaviourist and next waypoint.
        ///</summary>
        private float nextWaypointDistance = 2f;

        /// <summary>
        /// Time to wait until next walking on path.
        ///</summary>
        private float waitTime = 2f;///////Move somewhere to edit from editor.

        /// <summary>
        /// Time since we reached the end of path.
        ///</summary>
        private float pathEndedTime;

        public Seek_Behav(ISeekBehaviourHandler behaviourist)
        {
            behavrst = behaviourist;
            currentAction = WaitForPath;
        }

        /// <summary>
        /// Pathseeker gets path in parallel. They say.
        ///</summary>
        public void Enter()
        {
            behavrst.PathSeeker.pathCallback += OnPathComplete;
            behavrst.PathSeeker.StartPath(behavrst.Rigidbody.position, behavrst.Target);
            currentAction = WaitForPath;
        }

        public void FixedUpdate()
        {
            currentAction();
        }

        public void Update()
        {

        }

        public void Exit()
        {
            currentPath = null;
            behavrst.PathSeeker.pathCallback -= OnPathComplete;
        }

        /// <summary>
        /// When we get fresh path.
        ///</summary>
        void OnPathComplete(Path p)
        {
            if (p.error)
            {
                Debug.Log(p.errorLog);
                behavrst.PathSeeker.StartPath(behavrst.Rigidbody.position, behavrst.Target, OnPathComplete);
                return;
            }
            currentPath = p;
            currentWaypoint = 0;
            currentAction = MoveToEndOfPath;
        }

        void WaitForPath() { }

        /// <summary>
        /// Just stay staring to nowhere. I'm always looking around that way too.
        ///</summary>
        void LookAround()
        {
            if (Time.time > pathEndedTime + waitTime)
            {
                Vector2 nextPosition = behavrst.Rigidbody.position + new Vector2(Random.value - .5f, Random.value - .5f) * 20f;
                behavrst.PathSeeker.StartPath(behavrst.Rigidbody.position, nextPosition);
                currentAction = WaitForPath;
            }
        }

        void MoveToEndOfPath()
        {
            bool pathEnded = false;
            float distanceToWaypoint;
            while (true)
            {
                distanceToWaypoint = Vector2.SqrMagnitude(behavrst.Rigidbody.position - (Vector2)currentPath.vectorPath[currentWaypoint]);
                if (distanceToWaypoint < nextWaypointDistance)
                {
                    if (currentWaypoint + 1 < currentPath.vectorPath.Count)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        pathEnded = true;
                        break;
                    }
                }
                else
                    break;
            }
            // float speedFactor = pathEnded ? distanceToWaypoint / nextWaypointDistance : 1f;
            Vector2 dir = ((Vector2)currentPath.vectorPath[currentWaypoint] - behavrst.Rigidbody.position).normalized;
            behavrst.Motor.MoveAndRotate(dir);
            if (pathEnded)
            {
                Reset();
            }
        }

        /// <summary>
        /// Reset to initial action.
        ///</summary>
        public void Reset()
        {
            pathEndedTime = Time.time;
            behavrst.Rigidbody.velocity = Vector2.zero;
            behavrst.Rigidbody.angularVelocity = 0f;
            currentAction = LookAround;
        }
    }
}