﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface ILookAroundBehavHandler
{
    IMotor Motor { get; }
    /// <summary>
    /// Target to look...maybe around.
    ///</summary>
    Vector2 Target { get; }

    /// <summary>
    /// Method for exiting behaviour.
    ///</summary>
    void LookAround_Behav_Ended();
}
