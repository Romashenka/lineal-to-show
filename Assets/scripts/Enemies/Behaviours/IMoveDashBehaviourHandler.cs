﻿using UnityEngine;

namespace BatProject.BehavioursSystem
{
    public interface IMoveDashBehaviourHandler
    {
        Rigidbody2D Rigidbody { get; }
        /// <summary>
        /// Target to move and dash.
        ///</summary>
        Vector2 Target { get; }
        IMotor Motor { get; }
    }
}