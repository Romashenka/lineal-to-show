﻿using UnityEngine;

namespace BatProject.BehavioursSystem
{
    public interface ISeekBehaviourHandler
    {
        Rigidbody2D Rigidbody { get; }
        /// <summary>
        /// Component of <see cref="AstarPath"/>-project external asset.
        ///</summary>
        Pathfinding.Seeker PathSeeker { get; }
        /// <summary>
        /// Last target position where we start to seek.
        ///</summary>
        Vector2 Target { get; }
        IMotor Motor { get; }
    }
}