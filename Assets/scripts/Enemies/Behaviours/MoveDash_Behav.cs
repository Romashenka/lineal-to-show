﻿using UnityEngine;


namespace BatProject.BehavioursSystem
{

    /// <summary>
    /// Aggressive behaviour, that move to target and dash.
    ///</summary>
    [System.Serializable]
    public class MoveDash_Behav : IBehavBase
    {
        /// <summary>
        /// Behaviourist.
        ///</summary>
        IMoveDashBehaviourHandler handler;
        /// <summary>
        /// Timer to switch between actions.
        ///</summary>
        float time;

        /////////////////Additive
        /////////////////Move somewhere to edit. Don't know where.
        public float stopTime = 5f;
        public float dashForce = 5f;
        public float dashTime = .3f;

        /// <summary>
        /// Current action.
        ///</summary>
        private System.Action currentBehavAction;

        ///<param name="handler"> object that behave this way </param>
        public MoveDash_Behav(IMoveDashBehaviourHandler handler)
        {
            this.handler = handler;
            currentBehavAction = MoveToTarget;
        }

        public void Enter()
        {

        }

        public void Exit()
        {

        }

        public void Update()
        {

        }

        public void FixedUpdate()
        {
            currentBehavAction();
        }

        /// <summary>
        /// Moves to target. If target close switch to rotate-action.
        ///</summary>
        void MoveToTarget()
        {
            Vector2 toTarget = handler.Target - handler.Rigidbody.position;
            handler.Motor.MoveAndRotate(toTarget.normalized);
            if (Vector2.SqrMagnitude(toTarget) < 5f)
            {
                currentBehavAction = RotateToTarget;
            }
        }
        /// <summary>
        /// Rotate to target. If target in front switch to dash-action
        ///</summary>
        void RotateToTarget()
        {
            Vector2 moveDirection = (handler.Target - handler.Rigidbody.position).normalized;
            handler.Motor.Rotate(moveDirection, 2f);
            Vector2 forward = handler.Rigidbody.transform.up;
            if (Vector2.SignedAngle(moveDirection, forward) < 5f)
            {
                currentBehavAction = Dash;
            }
        }

        /// <summary>
        /// Dash and rest for a while.
        ///</summary>
        void Dash()
        {
            Vector2 force = ((Vector2)handler.Target - handler.Rigidbody.position).normalized * dashForce;
            handler.Rigidbody.AddForce(force, ForceMode2D.Impulse);
            currentBehavAction = Wait;
            time = Time.time;
        }

        /// <summary>
        /// Wait and dash again or move to target that is too far.
        ///</summary>
        void Wait()
        {
            //handler.Motor.Rotate(handler.Rigidbody.velocity);
            if (Time.time > time + dashTime)
            {
                handler.Rigidbody.velocity = Vector2.MoveTowards(handler.Rigidbody.velocity, Vector2.zero, stopTime * Time.deltaTime);
            }
            if (Vector2.SqrMagnitude(handler.Rigidbody.velocity) < .5f)
            {
                handler.Rigidbody.velocity = Vector2.zero;
                currentBehavAction = MoveToTarget;
            }
        }
    }
}
