﻿using UnityEngine;

namespace BatProject.BehavioursSystem
{
    /// <summary>
    /// Simple behaviour to look at the target and wait for a while.
    ///</summary>
    public class LookAround_Behav : IBehavBase
    {
        /// <summary>
        /// Behaviourist.
        ///</summary>
        private ILookAroundBehavHandler handler;
        /// <summary>
        /// Total time in behaviour.
        ///</summary>
        private float behaviourTime;
        /// <summary>
        /// Time of begining the behaviour.
        ///</summary>
        private float startTime;

        ///<param name="behaviourist"> object that behaves that way</param>
        ///<param name="behaviourTime"> time till the end of behaviour</param>
        public LookAround_Behav(ILookAroundBehavHandler behaviourist, float behaviourTime)
        {
            handler = behaviourist;
            this.behaviourTime = behaviourTime;
        }

        public void Enter() { startTime = Time.time; }

        public void Exit() { }

        public void FixedUpdate()
        {
            Look();
        }

        public void Update()
        {

        }

        void Look()
        {
            handler.Motor.Rotate(handler.Target);
            if (Time.time > startTime + behaviourTime)
            {
                handler.LookAround_Behav_Ended();
            }
        }
    }
}