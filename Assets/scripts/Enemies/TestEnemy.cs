﻿using UnityEngine;
using BatProject.GameSaves;
using BatProject.Stats;

namespace BatProject.Enemies
{
    /// <summary>
    /// Testing enemy.
    ///</summary>
    public class TestEnemy : Unit, GameSaves.ISavable
    {
        public GameObject cadaver;

        public Receptors.FieldOfViewObsCheck fieldOfView;

        public EnemyStats statsStatic;

        public override IUnitDynamicStats statsDynamic { get { return _statsDynamic; } }

        private UnitDynamicStats _statsDynamic;

        public Rigidbody2D rb { get; private set; }

        public Pathfinding.Seeker PathSeeker { get; private set; }

        private BehavioursSystem.TestEnemyBehavMachine machine;

        void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            if (statsStatic is IUnitDynamicStatsInitializer)
                _statsDynamic = new UnitDynamicStats(statsStatic as IUnitDynamicStatsInitializer);

            PathSeeker = GetComponent<Pathfinding.Seeker>();

            machine = new BehavioursSystem.TestEnemyBehavMachine(this);
            fieldOfView.enter += machine.OnTriggerEnter2D;
            fieldOfView.exit += machine.OnTriggerExit2D;
        }

        void FixedUpdate()
        {
            machine.FixedUpdate();
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            machine.OnCollisionEnter(collision);
        }

        public override void GetImpact(Vector2 point, Vector2 direction)
        {
            
        }


        /// <summary>
        /// For save-load system.
        ///</summary>
        public SavedData Save()
        {
            return new TestEnemySavedDat(transform.position);
        }

        /// <summary>
        /// For save-load system.
        ///</summary>
        public bool Load(SavedData saved)
        {
            if (saved == null)
            {
                Destroy(gameObject);
                return true;
            }
            TestEnemySavedDat tESD = saved as TestEnemySavedDat;
            if (tESD != null)
            {
                transform.position = (Vector2)tESD.position;
                return true;
            }
            else
            {
                return false;
            }
        }


        protected override void Die()
        {
            Destroy(gameObject);
            Instantiate(cadaver, transform.position, Quaternion.identity);
        }
    }
}
