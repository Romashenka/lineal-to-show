﻿using UnityEngine;

/// <summary>
/// Interface to all kins of Motors.
///</summary>
public interface IMotor
{
    Vector2 Velocity { get; }
    void Move(Vector2 direction, float speedMultiplyer);
    void Move(Vector2 direction);
    void Rotate(Vector2 direction, float speedMultiplyer);
    void Rotate(Vector2 direction);
    void MoveAndRotate(Vector2 direction, float speedMultiplyer);
    void MoveAndRotate(Vector2 direction);
    void Stop();
}
