﻿using BatProject.Stats;
using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Component, that calls GetDamage methon on gameobject with "Player" tag on collision.
    ///</summary>
    public class DamagePlayerOnCollision : MonoBehaviour
    {
        public EnemyStats stats;

        void OnCollisionEnter2D(Collision2D coll)
        {
            if (coll.gameObject.CompareTag("Player"))
            {
                Unit unit = coll.gameObject.GetComponent<Unit>();
                unit.GetDamage(stats.damage);
                unit.GetImpact(coll.contacts[0].point, Vector2.down);
            }
        }
    }
}