﻿using UnityEngine;


namespace BatProject.BehavioursSystem
{
    /// <summary>
    /// Test behaviour machine for test enemy, that controls test behaviours with test acts.
    ///</summary>
    public class TestEnemyBehavMachine : BehavMachineBase, IMoveDashBehaviourHandler, ISeekBehaviourHandler, ILookAroundBehavHandler
    {
        /// <summary>
        /// Enemy, on what this machine is executed.
        ///</summary>
        private Enemies.TestEnemy testEnemy;

        /// <summary>
        /// Transform of enemy's target. Player or whatever.
        ///</summary>
        private Transform targetTransform;
        /// <summary>
        /// Last position the enemy saw the target.
        ///</summary>
        private Vector2 lastTargetPos = Vector2.zero;
        public Vector2 Target
        {
            get
            {
                if (targetTransform != null)
                {
                    lastTargetPos = targetTransform.position;
                    return targetTransform.position;
                }
                else
                {
                    return lastTargetPos;
                }
            }
        }

        private MotorRB _motor;
        public IMotor Motor { get { return _motor as IMotor; } }
        public Pathfinding.Seeker PathSeeker { get { return testEnemy.PathSeeker; } }
        public Rigidbody2D Rigidbody { get { return testEnemy.rb; } }

        private MoveDash_Behav moveDash_Behav;
        private Seek_Behav seek_Behav;
        private LookAround_Behav lookAround_Behav;

        public TestEnemyBehavMachine(Enemies.TestEnemy enemy)
        {
            testEnemy = enemy;
            lastTargetPos = enemy.transform.position;

            moveDash_Behav = new MoveDash_Behav(this as IMoveDashBehaviourHandler);
            seek_Behav = new Seek_Behav(this as ISeekBehaviourHandler);
            lookAround_Behav = new LookAround_Behav(this as ILookAroundBehavHandler, 3f);

            currentBehaviour = seek_Behav;
            _motor = new MotorRB(enemy.rb, enemy.statsDynamic as ISpeedRateHolder, enemy.statsStatic as IMotorStats);
        }

        public void OnTriggerEnter2D(Collider2D coll)
        {
            targetTransform = coll.transform;
            currentBehaviour = moveDash_Behav;
        }

        public void OnTriggerExit2D(Collider2D coll, Receptors.FieldOfViewObsCheck.ExitMode exitMode)
        {
            targetTransform = null;
            currentBehaviour = seek_Behav;
        }

        public void OnCollisionEnter(Collision2D collision)
        {
            if (currentBehaviour == seek_Behav)
            {
                lastTargetPos = collision.transform.position;
                currentBehaviour = lookAround_Behav;
            }
        }

        public void LookAround_Behav_Ended()
        {
            currentBehaviour = seek_Behav;
        }
    }
}
