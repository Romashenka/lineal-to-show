﻿
/// <summary>
/// Interface that describes static data for Motor-objects.
///</summary>
public interface IMotorStats
{
    float moveSpeed { get; }
    float rotationSpeed { get; }
}
