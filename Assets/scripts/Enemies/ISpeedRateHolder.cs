﻿

/// <summary>
/// Interface that describes speedRate - dynamic data.
///</summary>
public interface ISpeedRateHolder
{
    float speedRate { get; }
}
