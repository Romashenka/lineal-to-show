﻿using UnityEngine;

/// <summary>
/// Motor-object that uses Rigidbody2D to act.
///</summary>
public class MotorRB : IMotor
{
    private Rigidbody2D myRb;
    private ISpeedRateHolder speedRateHolder;
    private IMotorStats staticStats;

    /// <summary>
    /// Motor-object that uses Rigidbody2D to act.
    ///</summary>
    ///<param name="rb"> Rigidbody that will be moved be this motor.</param>
    ///<param name="speedRateHolder"> Object that contains speed rate variable.</param>
    ///<param name="motorStats"> Object that contains move speed and rotation speed.</param>
    public MotorRB(Rigidbody2D rb, ISpeedRateHolder speedRateHolder, IMotorStats motorStats)
    {
        if (rb == null)
        {
            Debug.Log("Rigidbody is null. Motor can't work.");
        }
        else if (speedRateHolder == null)
        {
            Debug.Log("ISpeedRateHolder speedRateHolder in null. Motor can't work.", rb.gameObject);
            rb.gameObject.SetActive(false);
        }
        else if (motorStats == null)
        {
            Debug.Log("IMotorStats motorStats is null. Motor can't work.", rb.gameObject);
            rb.gameObject.SetActive(false);
        }
        myRb = rb;
        this.speedRateHolder = speedRateHolder;
        staticStats = motorStats;
    }

    public Vector2 Velocity { get { return myRb.velocity; } }

    public void Move(Vector2 direction, float speedMultiplyer)
    {
        myRb.velocity = direction * staticStats.moveSpeed * speedMultiplyer * speedRateHolder.speedRate;
    }

    public void Move(Vector2 direction)
    {
        Move(direction, 1f);
    }

    public void Rotate(Vector2 direction, float speedMultiplyer)
    {
        float rotatAng = Vector2.SignedAngle(Vector2.up, direction);
        myRb.rotation = Mathf.MoveTowardsAngle(myRb.rotation, rotatAng, staticStats.rotationSpeed * speedMultiplyer * speedRateHolder.speedRate);
    }

    public void Rotate(Vector2 direction)
    {
        Rotate(direction, 1f);
    }

    public void MoveAndRotate(Vector2 direction, float speedMultiplyer)
    {
        Move(direction, speedMultiplyer);
        Rotate(direction, speedMultiplyer);
    }

    public void MoveAndRotate(Vector2 direction)
    {
        MoveAndRotate(direction, 1f);
    }

    /// <summary>
    /// Stops immediatly.
    ///</summary>
    public void Stop()
    {
        myRb.velocity = Vector2.zero;
    }
}
