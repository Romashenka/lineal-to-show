﻿using System;

namespace BatProject.Player
{
    public class PlayerState
    {
        public event Action<PlayerState> playerStateChanged;

        private bool _shootingEnabled = true;
        public bool shootingEnabled
        {
            get { return _shootingEnabled; }
            set { TryToChange(ref _shootingEnabled, value); }
        }

        private bool _movingEnabled = true;
        public bool movingEnabled
        {
            get { return _movingEnabled; }
            set { TryToChange(ref _movingEnabled, value); }
        }

        void TryToChange(ref bool from, bool to)
        {
            if (from != to)
            {
                from = to;
                if (playerStateChanged != null)
                    playerStateChanged(this);
            }
        }

        
    }
}