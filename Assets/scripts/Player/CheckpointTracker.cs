﻿using BatProject.Player;
using BatProject.UI;
using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Component that sets <see cref="DirectionToCheckpoint"> prefabs - visual representations of directions to checkpoints.
    /// And hooks to input.
    /// </summary>
    public class CheckpointTracker : MonoBehaviour
    {
        /// <summary>
        /// Prefab of visual representation of direction.
        ///</summary>
        public DirectionToCheckpoint arrowPrefab;
        public InputController inputController;

        void Awake()
        {
            CheckPoint[] checkPoints = FindObjectsOfType<CheckPoint>();
            foreach (var point in checkPoints)
            {
                DirectionToCheckpoint arrow = Instantiate(arrowPrefab, transform);
                arrow.center = transform;
                arrow.checkpoint = point.transform;
                point.checkpointDestroyed += () => Destroy(arrow.gameObject);
            }
            inputController.showCheckpointTracker += TurnOn;
            inputController.hideCheckpointTracker += TurnOff;
            gameObject.SetActive(false);
        }

        void TurnOn()
        { gameObject.SetActive(true); }

        void TurnOff()
        { gameObject.SetActive(false); }

        void OnDestroy()
        {
            inputController.showCheckpointTracker -= TurnOn;
            inputController.hideCheckpointTracker -= TurnOff;
        }
    }
}