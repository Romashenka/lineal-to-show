﻿using UnityEngine;
using BatProject.Items;
using System;
using System.Collections.Generic;

namespace BatProject.Player
{
    [System.Serializable]
    public class PlayerShooter
    {
        public ActionSet firstActSet { get; private set; }
        public ActionSet secondActSet { get; private set; }
        private PlayerAffector affector;
        private PlayerEquipment equipment;
        private EquipmentWeapon currentEquipped;
        public IUnitDynamicStats playerDynStats { get; private set; }
        public WeaponPlace weaponPlace { get; private set; }
        public Aim aim { get; private set; }

        private bool active = true;
        private bool aiming = false;


        public Vector2 origin { get { return weaponPlace.gunTip.position; } }
        public AudioSource audioSource { get { return weaponPlace.audioSource; } }

        public PlayerShooter(PlayerController controller)
        {
            firstActSet = new ActionSet(Controls.buttons.fire);
            secondActSet = new ActionSet(Controls.buttons.secondaryFire);

            controller.state.playerStateChanged += OnPlayerStateChanged;
            aim = controller.aim;
            equipment = controller.equipment;
            playerDynStats = controller.statsDynamic;
            weaponPlace = controller.weaponPlace;
            affector = controller.affector;

            currentEquipped = equipment.currentEquippedWeapon;
            currentEquipped.weapon.SetAim(aim);
            currentEquipped.weapon.SetWeapon(this);
            currentEquipped.weaponShot += OnWeaponShot;
            weaponPlace.audioSource.clip = currentEquipped.weapon.shotSound;
        }


        public void HandleInput()
        {
            if (!active)
                return;

            firstActSet.HandleInput(this, currentEquipped);
            secondActSet.HandleInput(this, currentEquipped);

            if (InputControl.GetButtonDown(Controls.buttons.switchWeapon))
            {
                SwitchWeapon();
            }

            if (InputControl.GetButtonDown(Controls.buttons.reload))
            {
                equipment.currentEquippedWeapon.Reload();
            }
        }

        void SwitchWeapon()
        {
            firstActSet.Clear();
            secondActSet.Clear();

            equipment.SwitchWeapon();
            currentEquipped.weaponShot -= OnWeaponShot;
            currentEquipped = equipment.currentEquippedWeapon;
            currentEquipped.weaponShot += OnWeaponShot;

            currentEquipped.weapon.SetAim(aim);
            currentEquipped.weapon.SetWeapon(this);
        }

        void OnWeaponShot(Vector2 direction)
        {
            Weapon current = currentEquipped.weapon;
            aim.OnWeaponShot();

            affector.TakeImpact(-direction * current.shotImpact);

            weaponPlace.audioSource.PlayOneShot(currentEquipped.weapon.shotSound);
        }

        void OnPlayerStateChanged(PlayerState state)
        {
            if (state.shootingEnabled)
            {
                active = true;
                aim.Hide(false);
            }
            else
            {
                active = false;
                aim.Hide(true);
                firstActSet.ResetChanges(this, currentEquipped);
                secondActSet.ResetChanges(this, currentEquipped);
            }
        }

        public void OnDestroy()
        {
            currentEquipped.weaponShot -= OnWeaponShot;
        }

        public class ActionSet
        {
            public ActionSet(KeyMapping keyButton)
            {
                this.keyButton = keyButton;
            }

            private KeyMapping keyButton;

            private event Action<PlayerShooter, EquipmentWeapon> buttonDown, buttonUp, button, resetChanges;

            public void SetButton(Action<PlayerShooter, EquipmentWeapon> onButton)
            {
                button = onButton;
            }

            public void SetButtonUp(Action<PlayerShooter, EquipmentWeapon> onButtonUp)
            {
                buttonUp = onButtonUp;
            }

            public void SetButtonDown(Action<PlayerShooter, EquipmentWeapon> onButtonDown)
            {
                buttonDown = onButtonDown;
            }

            public void AddOnReset(Action<PlayerShooter, EquipmentWeapon> onReset)
            {
                resetChanges += onReset;
            }

            public void ResetChanges(PlayerShooter shooter, EquipmentWeapon equipment)
            {
                if (resetChanges != null)
                    resetChanges(shooter, equipment);
            }



            public void Clear()
            {
                buttonDown = null;
                buttonUp = null;
                button = null;
                resetChanges = null;
            }

            public void HandleInput(PlayerShooter shooter, EquipmentWeapon equ)
            {
                if (InputControl.GetButtonDown(keyButton) && buttonDown != null)
                {
                    buttonDown(shooter, equ);
                }
                if (InputControl.GetButtonUp(keyButton) && buttonUp != null)
                {
                    buttonUp(shooter, equ);
                }
                if (InputControl.GetButton(keyButton) && button != null)
                {
                    button(shooter, equ);
                }
            }
        }

    }
}