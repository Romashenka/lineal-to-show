﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using BatProject.Receptors;


namespace BatProject
{
    /// <summary>
    /// Component that provides logic to keep track of targets in field of view and find closest from them. <see cref="FieldOfView"/>.
    ///</summary>
    [RequireComponent(typeof(FieldOfView))]
    public class AutoAimSystem : MonoBehaviour
    {
        /// <summary>
        /// <see cref="System.Action"/> event that called when target enters field of view.
        ///</summary>
        public Action targetCaptured;

        /// <summary>
        /// <see cref="System.Action"/> event that called when target exits field of view.
        ///</summary>
        public Action targetsLost;
        private FieldOfView fieldOfView;
        private List<Transform> targets;

        /// <summary>
        /// Does it sees something.
        ///</summary>
        public bool captured { get; private set; }
        void Start()
        {
            fieldOfView = GetComponent<FieldOfView>();
            fieldOfView.enter += (Collider2D coll) => CaptureTarget(coll.transform);
            fieldOfView.exit += (Collider2D coll) => LooseTarget(coll.transform);
            targets = new List<Transform>();
        }

        void CaptureTarget(Transform targ)
        {
            targets.Add(targ);
            if (targets.Count == 1)
            {
                captured = true;
                if (targetCaptured != null)
                {
                    targetCaptured();
                }
            }
        }

        void LooseTarget(Transform targ)
        {
            targets.Remove(targ);
            if (targets.Count == 0)
            {
                captured = false;
                if (targetsLost != null)
                {
                    targetsLost();
                }
            }
        }

        /// <summary>
        /// Closest target to this.position. If none returns null.
        /// </summary>
        public Transform ClosestTarget()
        {
            if (targets.Count == 0)
                return null;

            //While triggerExit-bug not fixed
            if (targets.Count == 1 && targets[0] == null)
            {
                LooseTarget(targets[0]);
                return transform;
            }

            float minDist = Mathf.Infinity;
            int closestIndex = 0;
            for (int i = 0; i < targets.Count; i++)
            {
                //While triggerExit-bug not fixed
                if (targets[i] == null)
                {
                    LooseTarget(targets[i]);
                    continue;
                }

                float dist = (targets[i].position - transform.position).sqrMagnitude;
                if (dist < minDist)
                {
                    minDist = dist;
                    closestIndex = i;
                }
            }
            return targets[closestIndex];
        }
    }
}