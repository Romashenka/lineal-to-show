﻿using UnityEngine;
using TMPro;

namespace BatProject.Player
{
    /// <summary>
    /// Component that shows direction from some <see cref="Transform"/> to checkpoint.
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public class DirectionToCheckpoint : MonoBehaviour
    {
        /// <summary>
        /// Distance from parent <see cref="Transform"/> - center to containing gameObject. 
        /// </summary>
        public float distanceFromCenter = 3f;

        /// <summary>
        /// From <see cref="Transform"/>.
        /// </summary>
        public Transform center { get; set; }
        /// <summary>
        /// To <see cref="Transform"/>.
        /// </summary>
        public Transform checkpoint { get; set; }
        private TextMeshPro textMesh;
        private SpriteRenderer spriteRenderer;

        void Start()
        {
            textMesh = GetComponentInChildren<TextMeshPro>();
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        void Update()
        {

            Vector3 direction = (checkpoint.position - center.position).normalized * distanceFromCenter;
            transform.position = center.transform.position + direction;
            float angle = Vector2.SignedAngle(Vector2.up, direction);
            transform.rotation = Quaternion.Euler(0f, 0f, angle);

            float distance = (int)Vector2.Distance(checkpoint.position, transform.position);
            SetDistText(distance.ToString());
            if (distance < 15f)
            {
                float alpha = distance / 15f;
                spriteRenderer.color = ChangeColorAlpha(spriteRenderer.color, alpha);
                textMesh.color = ChangeColorAlpha(textMesh.color, alpha);
            }
        }

        void SetDistText(string text)
        {
            textMesh.text = text + " m";
        }

        Color ChangeColorAlpha(Color c, float newAlpha)
        {
            c.a = newAlpha;
            return c;
        }
    }
}