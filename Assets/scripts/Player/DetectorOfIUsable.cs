﻿using System.Collections.Generic;
using UnityEngine;

namespace BatProject
{

    /// <summary>
    /// Component that detects gameobjects with <see cref="IUsable"/> components and uses it on input.
    ///</summary>
    [RequireComponent(typeof(Collider2D))]
    public class DetectorOfIUsable : MonoBehaviour
    {
        public Player.PlayerController player;
        /// <summary>
        /// IDs of gameobjects that have been detected.
        ///</summary>
        private List<int> ids;

        /// <summary>
        /// <see cref="IUsables"/> components that have been detected. Parallel with "ids" list.
        ///</summary>
        private List<IUsable> usables;

        /// <summary>
        /// On we are near some item that we could use.
        ///</summary>
        public event System.Action<IUsable> usableDetected;

        /// <summary>
        /// On all items that we could use are gone.
        ///</summary>
        public event System.Action usablesGone;

        void Start()
        {
            ids = new List<int>();
            usables = new List<IUsable>();
        }

        void Update()
        {
            if (InputControl.GetButtonDown(Controls.buttons.use))
            {
                UseLastUsable();
            }
        }

        /// <summary>
        /// Check if entered item has <see cref="IUsable"/> and if it has, adds it to track-lists.
        ///</summary>
        void OnTriggerEnter2D(Collider2D collider)
        {
            int instanceID = collider.GetInstanceID();
            if (ids.Contains(instanceID))
                return;

            IUsable usable = collider.GetComponent<IUsable>();
            if (usable != null)
            {
                usable.OnDetected();
                ids.Add(instanceID);
                usables.Add(usable);
                if (usableDetected != null)
                    usableDetected(usable);
            }
        }

        /// <summary>
        /// Checks if exited item has <see cref="IUsable"/> and, if it has, removes it frome track-lists.
        ///</summary>
        void OnTriggerExit2D(Collider2D collider)
        {
            int instanceID = collider.GetInstanceID();

            int index = ids.FindIndex((int id) => id == instanceID);

            if (index < 0)
                return;

            ids.RemoveAt(index);
            usables[index].OnLost();
            usables.RemoveAt(index);

            if (usables.Count == 0 && usablesGone != null)
                usablesGone();
        }

        /// <summary>
        /// Uses first from end <see cref="IUsable"/> from track-lists.
        ///</summary>
        public void UseLastUsable()
        {
            if (usables.Count != 0)
                player.UseItem(usables[usables.Count - 1]);
        }
    }
}