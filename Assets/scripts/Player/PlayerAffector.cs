﻿using UnityEngine;
using BatProject.Stats;

namespace BatProject.Player
{
    public class PlayerAffector
    {
        private Camera mainCamera;
        private Aim aim;
        private PlayerMotor motor;
        private PlayerStats stats;

        public PlayerAffector(PlayerController controller)
        {
            mainCamera = Camera.main;
            aim = controller.aim;
            motor = controller.motor;
            stats = controller.playerStaticStats;
        }

        public void TakeImpact(Vector2 impactVector)
        {
            mainCamera.transform.position += (Vector3)impactVector;
        }

        public void Update()
        {
            if (motor.moving)
            {
                aim.OnMoving();
            }
        }
    }
}