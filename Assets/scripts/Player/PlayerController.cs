﻿using System;
using BatProject.GameSaves;
using BatProject.Stats;
using UnityEngine;

namespace BatProject.Player
{
    public class PlayerController : Unit, GameSaves.ISavable
    {
        [SerializeField]
        private WeaponPlace _weaponPlace;
        public WeaponPlace weaponPlace { get { return _weaponPlace; } }

        [SerializeField]
        private Aim _aim;
        public Aim aim { get { return _aim; } }

        [SerializeField]
        private PlayerDynamicStats playerDynamicStats;

        [SerializeField]
        private PlayerStats _playerStaticStats;
        public PlayerStats playerStaticStats { get { return _playerStaticStats; } }

        [SerializeField]
        private PlayerEquipment _equipment;
        public PlayerEquipment equipment { get { return _equipment; } }

        public PlayerAffector affector { get; private set; }

        public Rigidbody2D myRB { get; private set; }

        private PlayerMotor _motor;
        public PlayerMotor motor { get { return _motor; } }
        private PlayerShooter shooter;

        public override IUnitDynamicStats statsDynamic { get { return playerDynamicStats; } }

        public PlayerState state { get; private set; }

        void Start()
        {
            myRB = GetComponent<Rigidbody2D>();
            if (playerStaticStats == null)
            {
                Debug.Log("No stats for PlayerController.", this);
            }
            playerDynamicStats.Init(playerStaticStats);
            state = new PlayerState();
            _motor = new PlayerMotor(this);
            affector = new PlayerAffector(this);
            _equipment.Init(this);
            shooter = new PlayerShooter(this);

            GlobalEvents.gamePaused += OnGamePaused;

        }

        void Update()
        {
            shooter.HandleInput();
            motor.HandleInput();
            affector.Update();
        }

        void FixedUpdate()
        {
            motor.FixedUpdate();
        }

        public void UseItem(IUsable usableItem)
        {
            usableItem.Use(this);
        }

        protected override void Die()
        {
            GlobalEvents.PlayerDead();
            Destroy(gameObject);
        }

        /// <summary>
        /// Interface for SavingSystem.
        ///</summary>
        public SavedData Save()
        {
            return new PlayerSaveData(playerDynamicStats.GetSaveData(), transform.position, transform.rotation);
        }

        /// <summary>
        /// Interface for SavingSystem.
        ///</summary>
        public bool Load(SavedData saved)
        {
            PlayerSaveData playerSaveData = saved as PlayerSaveData;
            if (playerSaveData != null)
            {
                playerDynamicStats.Init(playerSaveData.unitDynamicStatsSave);
                transform.position = (Vector2)playerSaveData.position;
                transform.rotation = (Quaternion)playerSaveData.rotation;
                return true;
            }
            else
            {
                return false;
            }
        }

        void OnGamePaused(bool paused) { enabled = !paused; }

        void OnDestroy()
        {
            GlobalEvents.gamePaused -= OnGamePaused;
            shooter.OnDestroy();
        }

        void OnDrawGizmos()
        {
            if (_aim != null)
                Gizmos.DrawRay(transform.position, _aim.DispersedDirection());
        }


        public void ChangeState(PlayerState state)
        { this.state = state; }

        public override void GetImpact(Vector2 point, Vector2 direction)
        {
            // throw new NotImplementedException();
            affector.TakeImpact(direction);
        }
    }
}