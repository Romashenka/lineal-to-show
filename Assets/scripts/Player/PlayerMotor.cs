﻿using BatProject.Stats;
using BatProject.Player;
using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Movement logic. 
    ///</summary>
    public class PlayerMotor
    {
        private Camera camera;
        private PlayerController player;
        private Rigidbody2D rigidbody;
        private PlayerStats playerStats;
        private IUnitDynamicStats statsDynamic;
        private Vector2 moveVector;
        public bool moving { get; private set; }

        public PlayerMotor(PlayerController target)
        {
            player = target;
            rigidbody = target.myRB;
            playerStats = target.playerStaticStats;
            statsDynamic = target.statsDynamic;
            camera = Camera.main;
            target.state.playerStateChanged += OnPlayerStateChange;
        }

        public void HandleInput()
        {
            moveVector =
                new Vector2(InputControl.GetAxisRaw(Controls.axes.horizontal), InputControl.GetAxisRaw(Controls.axes.vertical));

            if (InputControl.GetButtonDown(Controls.buttons.run))
            {
                player.state.shootingEnabled = false;
                statsDynamic.speedRate *= playerStats.runSpeedRate;
            }

            if (InputControl.GetButtonUp(Controls.buttons.run))
            {
                player.state.shootingEnabled = true;
                statsDynamic.speedRate /= playerStats.runSpeedRate;
            }

            if (moveVector == Vector2.zero)
            {
                player.state.shootingEnabled = true;
            }
        }

        public void FixedUpdate()
        {
            Move(moveVector);
            Vector2 lookDirection = Input.mousePosition - camera.WorldToScreenPoint(rigidbody.position);
            RotateTo(lookDirection);
        }

        void RotateTo(Vector2 direction)
        {
            if (direction == Vector2.zero)
                return;
            float targetRotation = Vector2.SignedAngle(Vector2.up, direction);
            float maxDegrDelta = playerStats.rotationSpeed * statsDynamic.speedRate * Time.deltaTime;
            rigidbody.rotation = Mathf.MoveTowardsAngle(rigidbody.rotation, targetRotation, maxDegrDelta);
        }

        void Move(Vector2 moveVector, float speedReductionRate = 1f)
        {
            if (moveVector != Vector2.zero)
            {
                moving = true;
            }
            else
            {
                moving = false;
            }
            moveVector *= playerStats.moveSpeed * statsDynamic.speedRate * speedReductionRate;
            float t = playerStats.acceleration * statsDynamic.speedRate * Time.deltaTime;
            rigidbody.velocity = Vector2.Lerp(rigidbody.velocity, moveVector, t);
        }

        void OnPlayerStateChange(PlayerState state)
        {

        }
    }
}