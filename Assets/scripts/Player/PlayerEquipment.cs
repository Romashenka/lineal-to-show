﻿using UnityEngine;
using BatProject.Items;
using System;

namespace BatProject.Player
{
    /// <summary>
    /// Class that defines player's weapons and defines methods to use them.
    ///</summary>
    [CreateAssetMenu(menuName = "Player/Equipment")]
    public class PlayerEquipment : ScriptableObject
    {
        /// <summary>
        /// Current equipped weapon changed to other.
        ///</summary>
        public event Action<int, EquipmentWeapon> weaponChanged;

        /// <summary>
        /// Equipped second weapon.
        ///</summary>
        public event Action<int> weaponSwitched;

        [SerializeField]
        private EquipmentWeapon[] _equipped;
        public EquipmentWeapon[] equipped { get { return _equipped; } }
        public int currentEquippedIndex { get; private set; }
        public EquipmentWeapon currentEquippedWeapon { get { return equipped[currentEquippedIndex]; } }


        public void Init(PlayerController player)
        {
            //For debug
            foreach (var item in equipped)
            {
                item.bulletsInClip = item.weapon.clipSize;
            }
        }

        public void TakeItem(EquipmentItem item)
        {
            if (item.itemType == ItemType.Weapon)
            {
                EquipmentWeapon weapon = item as EquipmentWeapon;
                if (weaponChanged != null)
                    weaponChanged(currentEquippedIndex, weapon);
            }
        }

        public void SwitchWeapon()
        {
            currentEquippedIndex = (currentEquippedIndex + 1) % equipped.Length;
            if (weaponChanged != null)
                weaponSwitched(currentEquippedIndex);
        }

    }
}