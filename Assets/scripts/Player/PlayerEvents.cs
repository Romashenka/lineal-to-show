﻿using System;
using System.Collections.Generic;
using UnityEngine;
using BatProject.Items;
using BatProject.Stats;

namespace BatProject.Player
{
    public class PlayerEvents : ScriptableObject
    {
        public event Action<float> healthChanged;
        public event Action<int, EquipmentWeapon> weaponChanged;
        public event Action weaponSwitched;


        public void Initialize(UnitDynamicStats dynamicStats)
        {
            dynamicStats.healthChanged += OnStatsHealthChanged;
            OnStatsHealthChanged(dynamicStats.health);
        }

        void OnStatsHealthChanged(float newHealth)
        {
            if (healthChanged != null)
                healthChanged(newHealth);
        }
    }
}