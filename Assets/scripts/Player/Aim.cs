﻿using UnityEngine;
using BatProject.Items;

namespace BatProject.Player
{
    public class Aim : MonoBehaviour
    {

        [SerializeField]
        private Transform origin;

        [SerializeField]
        private Transform[] children;

        [SerializeField]
        private float shrinkSpeed = 3f;

        [SerializeField]
        private float maxExtension = 5f;

        [SerializeField]
        private float minExtension = 1f;

        public float weaponDispersion { get; set; }
        public float currentExtension { get; private set; }
        private float onShotExtension;
        private float onMoveExtension;

        private Camera mainCamera;
        private Vector2[] childrenDirections;

        public bool hidden { get; private set; }

        public Weapon.AimStats aimStats
        {
            set
            {
                minExtension = value.minExtension;
                maxExtension = value.maxExtension;
                weaponDispersion = value.weaponDispersion;
                onShotExtension = value.onShotExtension;
                onMoveExtension = value.onMoveExtension;
            }
        }

        void Start()
        {
            mainCamera = Camera.main;
            Cursor.visible = false;
            GlobalEvents.gamePaused += Hide;

            childrenDirections = new Vector2[children.Length];
            for (int i = 0; i < children.Length; i++)
            {
                childrenDirections[i] = (children[i].position - transform.position) /
                                        Vector2.Distance(transform.position, children[i].position);
            }

            GlobalEvents.playerDead += OnPlayerDead;
        }

        void Update()
        {
            transform.position = (Vector2)mainCamera.ScreenToWorldPoint((Vector2)Input.mousePosition);
            float distFromOriginSqr = Vector2.SqrMagnitude(transform.position - origin.position);

            //weaponDispersion is applyed to 10f distance. Sqr here.
            float targetExtension = Mathf.LerpUnclamped(0f, weaponDispersion, distFromOriginSqr / 100f);
            targetExtension = Mathf.Clamp(targetExtension, minExtension, maxExtension);

            if (currentExtension != targetExtension)
            {
                currentExtension = Mathf.Lerp(currentExtension, targetExtension, shrinkSpeed * Time.deltaTime);
            }

            for (int i = 0; i < children.Length; i++)
            {
                children[i].localPosition = (Vector3)childrenDirections[i] * (currentExtension / 2f);
            }
        }

        public void OnMoving() { currentExtension += onMoveExtension * Time.deltaTime; }

        public void OnWeaponShot() { currentExtension += onShotExtension; }

        public Vector2 DispersedDirection()
        {
            Vector2 fromOrigin = (Vector2)transform.position - (Vector2)origin.position;
            float rand = Random.Range(-currentExtension / 2, currentExtension / 2);
            Vector2 dispersedPoint = Vector2.Perpendicular(fromOrigin) / fromOrigin.magnitude * rand;
            Vector2 dispersedFromOrigin = fromOrigin + dispersedPoint;

            return dispersedFromOrigin / dispersedFromOrigin.magnitude;
        }


        public void Hide(bool hide)
        {
            if (gameObject.activeSelf == hide)
            {
                gameObject.SetActive(!hide);
                if (!hide)
                    Update();
            }
        }

        void OnPlayerDead()
        {
            Destroy(gameObject);
            Cursor.visible = true;
        }

        void OnDestroy()
        {
            GlobalEvents.gamePaused -= Hide;
            GlobalEvents.playerDead -= OnPlayerDead;
        }
    }
}