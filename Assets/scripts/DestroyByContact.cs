﻿using UnityEngine;

namespace BatProject
{
    /// <summary>
    /// Component, that destroys gameObject OnCollisionEnter.
    /// </summary>
    public class DestroyByContact : MonoBehaviour
    {

        void OnCollisionEnter2D(Collision2D col)
        {
            Destroy(col.gameObject);
        }
    }
}