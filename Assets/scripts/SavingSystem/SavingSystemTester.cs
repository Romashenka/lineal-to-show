﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace BatProject.GameSaves
{
    /// <summary>
    ///Component for testing save-system by pressing L-button to save and K-button to load.
    /// </summary>
    public class SavingSystemTester : MonoBehaviour
    {
        SavingSystem ss;

        void Start()
        {
            ss = GetComponent<SavingSystem>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                ss.SaveGame();
            }
            if (Input.GetKeyDown(KeyCode.K))
            {
                Scene scene = SceneManager.GetActiveScene();
                SceneManager.LoadScene(scene.name);
            }
        }
    }
}
