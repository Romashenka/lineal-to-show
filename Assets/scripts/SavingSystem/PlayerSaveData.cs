﻿using UnityEngine;
using BatProject.Stats;

namespace BatProject.GameSaves
{
    /// <summary>
    /// Class-container of savable information about player.
    /// </summary>
    [System.Serializable]
    public class PlayerSaveData : SavedData
    {
        public UnitDynamicStats.SaveData unitDynamicStatsSave { get; private set; }
        public Util.Vector2Serializable position { get; private set; }
        public Util.QuaternionSerializable rotation { get; private set; }

        public PlayerSaveData(UnitDynamicStats.SaveData uDSSaveDat, Vector2 position, Quaternion rotation)
        {
            unitDynamicStatsSave = uDSSaveDat;
            this.position = position;
            this.rotation = rotation;
        }
    }
}