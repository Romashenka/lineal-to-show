﻿using UnityEngine;

namespace BatProject.GameSaves
{
    /// <summary>
    ///Class-container of savable information about TestEnemy.
    /// </summary>
    [System.Serializable]
    public class TestEnemySavedDat : SavedData
    {
        public Util.Vector2Serializable position { get; private set; }

        public TestEnemySavedDat(Vector2 position)
        {
            this.position = position;
        }
    }
}