﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BatProject.GameSaves
{

    /// <summary>
    ///Component, that defines serialization/deserialization of savable data and SaveGame/LoadGame calls.
    /// </summary>
    public class SavingSystem : MonoBehaviour
    {

        /// <summary>
        /// Objects that's state must be saved and load.
        /// </summary>
        public List<GameObject> gameObjectsToSave;

        /// <summary>
        /// Components of gameObjectsToSave that must be saved.
        /// </summary>
        private List<ISavable> savableComponents;

        /// <summary>
        /// Path of save-file of current scene.
        /// </summary>
        private static string currentSceneSavePath
        {
            get
            {
                Scene current = SceneManager.GetActiveScene();
                return System.String.Format("{0}/{1}.dat", saveDirectory, current.name);
            }
        }

        /// <summary>
        /// Directory with all save-files.
        /// </summary>
        private static string saveDirectory
        {
            get
            {
                string path = System.String.Format("{0}/saves", Application.persistentDataPath);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                return path;
            }
        }


        void Start()
        {
            savableComponents = new List<ISavable>();
            foreach (GameObject obj in gameObjectsToSave)
            {
                savableComponents.AddRange(obj.GetComponentsInChildren<ISavable>());
            }
            LoadGame();
        }

        public void SaveGame()
        {
            List<SavedData> extracted = ExtractData(savableComponents);
            Serialize(extracted);
        }

        public void LoadGame()
        {

            if (File.Exists(currentSceneSavePath))
            {
                List<SavedData> deserializedData = Deserialize();
                SetData(deserializedData, savableComponents);
            }
            else
            {
                Debug.Log("No load file. Scene starts clean.");
            }

            GlobalEvents.GameLoaded();
        }


        void SetData(List<SavedData> saved, List<ISavable> savables)
        {
            for (int i = 0; i < savables.Count; i++)
            {
                bool succeed = savableComponents[i].Load(saved[i]);
                if (!succeed)
                    Debug.Log("Object has problems with loading.", savableComponents[i].gameObject);
            }
        }


        /// <summary>
        /// Extracts savableData from savable components.
        /// SavedData will be null if component is destroyed.
        /// </summary>
        List<SavedData> ExtractData(List<ISavable> savables)
        {
            List<SavedData> savedDatas = new List<SavedData>(savableComponents.Count);
            foreach (ISavable savable in savableComponents)
            {
                // Some destroyed objects appear to be ==null - false, but Equals(null) - true. 
                // Because of Unity overrides == operator.
                if (savable != null && !savable.Equals(null))
                {
                    SavedData s = savable.Save();
                    savedDatas.Add(s);
                }
                else
                {
                    savedDatas.Add(null);
                }
            }
            return savedDatas;
        }


        void Serialize(System.Object toSetialize)
        {
            using (FileStream fs = new FileStream(currentSceneSavePath, FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(fs, toSetialize);
            }
        }

        List<SavedData> Deserialize()
        {
            List<SavedData> list = new List<SavedData>();

            using (FileStream fs = new FileStream(currentSceneSavePath, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                list = (List<SavedData>)formatter.Deserialize(fs);
            }
            return list;
        }


        /// <summary>
        ///Deletes save file for that level and resets all progress as consequence.
        /// </summary>
        public static void ResetProgressInLevel()
        {
            File.Delete(currentSceneSavePath);
        }

        /// <summary>
        ///Deletes all save files and resets all progress in the game as consequence.
        /// </summary>
        public static void ResetAllProgress()
        {
            try
            {
                DirectoryInfo directory = new DirectoryInfo(saveDirectory);
                FileInfo[] files = directory.GetFiles();
                foreach (FileInfo file in files)
                {
                    file.Delete();
                }

            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }
}
