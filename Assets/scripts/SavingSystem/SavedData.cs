﻿using UnityEngine;

namespace BatProject.GameSaves
{
    /// <summary>
    ///Base class for all savable data classes.
    /// </summary>
    [System.Serializable]
    public class SavedData
    {
        // public bool isAlive { get; private set; }

        // public SavedData(bool isAlive)
        // {
        //     this.isAlive = isAlive;
        // }
    }

}