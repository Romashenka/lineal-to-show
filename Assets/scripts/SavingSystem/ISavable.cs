﻿using UnityEngine;

namespace BatProject.GameSaves
{
    /// <summary>
    /// Interface for classes that can be saved by SaveSystem.
    /// </summary>
    public interface ISavable
    {
        GameObject gameObject { get; }
        SavedData Save();
        bool Load(SavedData saved);
    }
}