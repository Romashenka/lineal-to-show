﻿using UnityEngine;
using System;
using BatProject.Stats;

namespace BatProject.Player
{
    [CreateAssetMenu(menuName = "Player/DynamicStats")]
    public class PlayerDynamicStats : ScriptableObject, IUnitDynamicStats
    {
        public event Action<float> healthChanged;

        private UnitDynamicStats stats;


        public float health { get { return stats.health; } set { stats.health = value; } }

        public float speedRate { get { return stats.speedRate; } set { stats.speedRate = value; } }

        public UnitDynamicStats.SaveData GetSaveData()
        {
            return stats.GetSavedData();
        }

        public void Init(IUnitDynamicStatsInitializer initializer)
        {
            stats = new UnitDynamicStats(initializer);
            SetStats();
        }

        public void Init(UnitDynamicStats.SaveData saveData)
        {
            stats = new UnitDynamicStats(saveData);
            SetStats();
        }

        void SetStats()
        {
            stats.healthChanged += OnHealthChanged;
            OnHealthChanged(stats.health);
        }

        void OnHealthChanged(float health)
        {
            if (healthChanged != null)
                healthChanged(health);
        }

    }
}