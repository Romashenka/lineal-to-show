﻿using UnityEngine;

namespace BatProject.Stats
{
    /// <summary>
    ///ScriptableObject that describes static stats of Enemy.
    /// </summary>
    [CreateAssetMenu(menuName = "StatsData/Enemy")]
    public class EnemyStats : ScriptableObject, IUnitDynamicStatsInitializer, IMotorStats
    {
        [Header("Enemy Stats")]
        [SerializeField]
        private float _moveSpeed = 1f;
        public float moveSpeed { get { return _moveSpeed; } }
        [SerializeField]
        private float _rotationSpeed = 50f;
        public float rotationSpeed { get { return _rotationSpeed; } }

        [SerializeField]
        private float _damage = 1f;
        public float damage { get { return _damage; } }

        [SerializeField]
        private float _initialHealth = 10f;
        public float initialHealth { get { return _initialHealth; } }
    }
}