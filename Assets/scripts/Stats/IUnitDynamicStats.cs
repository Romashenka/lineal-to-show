﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BatProject
{
    public interface IUnitDynamicStats
    {
        float health { get; set; }
        float speedRate { get; set; }
    }
}