﻿using UnityEngine;
using BatProject.Items;

namespace BatProject.Stats
{
    /// <summary>
    ///ScriptableObject that describes static stats of Player.
    /// </summary>
    [CreateAssetMenu(menuName = "StatsData/Player")]
    public class PlayerStats : ScriptableObject, IUnitDynamicStatsInitializer
    {
        [Header("Player Stats")]
        [SerializeField]
        private float _rotationSpeed = 720f;
        public float rotationSpeed { get { return _rotationSpeed; } }

        [SerializeField]
        private float _moveSpeed = 5f;
        public float moveSpeed { get { return _moveSpeed; } }
        [Range(1f, 2f)]
        [SerializeField]
        private float _runSpeedRate = 1.5f;
        public float runSpeedRate { get { return _runSpeedRate; } }
        [SerializeField]
        private float _acceleration = 5f;
        public float acceleration { get { return _acceleration; } }

        [SerializeField]
        private float _initialHealth = 100f;
        public float initialHealth { get { return _initialHealth; } }

        [SerializeField]
        private float _moveAimImpact = .3f;
        public float moveAimImpact { get { return _moveAimImpact; } }

        [SerializeField]
        private float _runAimImpact = .5f;
        public float runAimImpact { get { return _runAimImpact; } }

        // [SerializeField]
        // private Weapon initialWeapon;
        // public Weapon InitialWeapon { get { return initialWeapon; } }

    }
}