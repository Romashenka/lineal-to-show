﻿
namespace BatProject.Stats
{
    /// <summary>
    /// Object that contains stats of <see cref="Unit"/>, that dynamically changing during the game.
    /// </summary>
    [System.Serializable]
    public class UnitDynamicStats : ISpeedRateHolder, IUnitDynamicStats
    {
        private float _health;
        public float health
        {
            get
            {
                return _health;
            }
            set
            {
                if (_health != value)
                {
                    if (healthChanged != null)
                        healthChanged(value);
                    _health = value;
                }
            }
        }
        public float speedRate { get; set; }

        /// <summary>
        /// If health value was changed.
        /// </summary>
        public event System.Action<float> healthChanged;

        public UnitDynamicStats(IUnitDynamicStatsInitializer stats)
        {
            health = stats.initialHealth;
            speedRate = 1f;
        }

       
        public UnitDynamicStats(SaveData savedData)
        {
            this.health = savedData.health;
            this.speedRate = savedData.speedRate;
        }

        public SaveData GetSavedData()
        {
            return new SaveData(health, speedRate);
        }

        [System.Serializable]
        public struct SaveData
        {
            public float health;
            public float speedRate;

            public SaveData(float health, float speedRate)
            {
                this.health = health;
                this.speedRate = speedRate;
            }
        }
    }
}