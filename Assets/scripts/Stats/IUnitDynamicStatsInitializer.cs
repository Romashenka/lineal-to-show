﻿

namespace BatProject.Stats
{
    /// <summary>
    ///Interface for objects that can initialize dynamic stats of <see cref="Unit">>.
    /// </summary>
    public interface IUnitDynamicStatsInitializer
    {
        float initialHealth { get; }
    }
}