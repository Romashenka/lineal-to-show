﻿using System;
using UnityEngine;
using BatProject.Player;

namespace BatProject.Items
{
    public abstract class WeaponAction : ScriptableObject
    {
        [SerializeField]
        protected bool fullAuto;
        public virtual void SetActions(PlayerShooter.ActionSet actionSet)
        {
            if (!fullAuto)
            {
                actionSet.SetButtonDown(Use);
            }
            else
            {
                actionSet.SetButton(Use);
            }
        }

        protected abstract void Use(PlayerShooter shooter, EquipmentWeapon equipmentWeapon);

    }
}