﻿using System;
using BatProject.Player;
using UnityEngine;

namespace BatProject.Items
{
    [CreateAssetMenu(menuName = "Weapons/WeaponActions/ShootProjectile")]
    public class ShootProjectile : ShootAction
    { 
        [SerializeField]
        private Projectile projectilePrefab;

        [SerializeField]
        private float damage = 10f;

        [SerializeField]
        private float projectileSpeed = 20f;

        [SerializeField]
        private float projectileLifeTime = 1f;

        protected override void Shoot(Vector2 origin, Vector2 direction)
        {
            Projectile projectile = Instantiate(projectilePrefab, origin, Quaternion.identity);
            projectile.damage = damage;
            projectile.AddForce(direction * projectileSpeed);
            Destroy(projectile.gameObject, projectileLifeTime);
        }
    }
}