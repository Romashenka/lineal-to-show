﻿using BatProject.Player;
using UnityEngine;

namespace BatProject.Items
{
    [CreateAssetMenu(menuName = "Weapons/WeaponActions/ShootBullet")]
    public class ShootBullet : ShootAction
    {
        [SerializeField]
        private LayerMask targets;

        [SerializeField]
        private float damage = 5f;

        [SerializeField]
        private float distance = 10f;

        [SerializeField]
        private Bullet bulletPrefab;

        [SerializeField]
        private float bulletLifetime = 1f;

        protected override void Shoot(Vector2 origin, Vector2 direction)
        {
            Bullet bullet = Instantiate(bulletPrefab, origin, Quaternion.identity);
            bullet.Shoot(origin, direction, distance, targets, damage);
            Destroy(bullet.gameObject, bulletLifetime);
        }

    }
}