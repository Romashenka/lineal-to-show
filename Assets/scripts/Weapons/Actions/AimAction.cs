﻿using BatProject.Player;
using UnityEngine;

namespace BatProject.Items
{
    [CreateAssetMenu(menuName = "Weapons/WeaponActions/AimAction")]
    public class AimAction : WeaponAction
    {
        [SerializeField]
        private float speedRateOnAiming = .5f;
        [SerializeField]
        private float dispersionRateOnAiming = .7f;

        private bool aiming;

        public override void SetActions(PlayerShooter.ActionSet actionSet)
        {
            AimAction newInstance = new AimAction();
            newInstance.speedRateOnAiming = speedRateOnAiming;
            newInstance.dispersionRateOnAiming = dispersionRateOnAiming;
            actionSet.SetButtonDown(newInstance.OnButtonDown);
            actionSet.SetButtonUp(newInstance.OnButtonUp);
            actionSet.AddOnReset(newInstance.Reset);
        }

        void OnButtonDown(PlayerShooter shooter, EquipmentWeapon equipmentWeapon)
        {
            if (aiming)
                return;

            shooter.playerDynStats.speedRate *= speedRateOnAiming;
            shooter.aim.weaponDispersion *= dispersionRateOnAiming;
            aiming = true;
        }

        void OnButtonUp(PlayerShooter shooter, EquipmentWeapon equipmentWeapon)
        {
            Reset(shooter, equipmentWeapon);
        }

        void Reset(PlayerShooter shooter, EquipmentWeapon equipmentWeapon)
        {
            if (!aiming)
                return;
            Weapon weapon = equipmentWeapon.weapon;
            shooter.playerDynStats.speedRate /= speedRateOnAiming;
            shooter.aim.weaponDispersion /= dispersionRateOnAiming;
            aiming = false;
        }

        protected override void Use(PlayerShooter shooter, EquipmentWeapon equipmentWeapon) { }
    }
}