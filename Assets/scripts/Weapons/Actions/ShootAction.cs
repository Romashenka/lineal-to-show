﻿using BatProject.Player;
using UnityEngine;

namespace BatProject.Items
{
    public abstract class ShootAction : WeaponAction
    {
        [SerializeField]
        private AudioClip onShot;

        [SerializeField]
        protected float shootRatePerSecond = 1f;

        [SerializeField]
        protected bool usesAmmo = true;

        protected override void Use(PlayerShooter shooter, EquipmentWeapon equipmentWeapon)
        {
            if (CanActCheck(equipmentWeapon))
            {
                if (usesAmmo)
                    equipmentWeapon.bulletsInClip--;
                equipmentWeapon.lastShootTime = Time.time;
                Vector2 direction = shooter.aim.DispersedDirection();
                Shoot(shooter.origin, direction);
                equipmentWeapon.OnWeaponShot(direction);
                shooter.audioSource.PlayOneShot(onShot);
            }
        }
 
        protected virtual bool CanActCheck(EquipmentWeapon equipmentWeapon)
        {
            bool ammoCheck = usesAmmo ? equipmentWeapon.bulletsInClip > 0 : true;
            return equipmentWeapon.lastShootTime + shootRatePerSecond < Time.time && ammoCheck;
        }

        protected abstract void Shoot(Vector2 origin, Vector2 direction);
    }
}