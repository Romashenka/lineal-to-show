﻿using UnityEngine;

namespace BatProject.Items
{
    public class Bullet : MonoBehaviour
    {
        protected RaycastHit2D hit;
        public virtual void Shoot(Vector2 origin, Vector2 direction, float distance, LayerMask targets, float damage)
        {
            hit = Physics2D.Raycast(origin, direction, distance, targets);
            if (hit.collider != null)
            {
                Unit unit = hit.collider.GetComponent<Unit>();
                if (unit != null)
                {
                    unit.GetDamage(damage);
                    unit.GetImpact(hit.point, direction);
                }
            }
        }
    }
}