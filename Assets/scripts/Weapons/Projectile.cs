﻿using UnityEngine;

namespace BatProject.Items
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Projectile : MonoBehaviour
    {
        private float _damage;
        public float damage { set { _damage = value; } }
        [SerializeField]
        private Rigidbody2D myRb;

        protected virtual void OnCollisionEnter2D(Collision2D coll)
        {
            Unit unit = coll.gameObject.GetComponent<Unit>();
            if (unit != null)
            {
                unit.GetDamage(_damage);
            }
            Destroy(gameObject);
        }

        public void AddForce(Vector2 force)
        {
            myRb.AddForce(force, ForceMode2D.Impulse);
        }
    }
}