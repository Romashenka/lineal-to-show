﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BatProject.Items
{
    public class LaserBullet : Bullet
    {
        public LineRenderer lineRenderer;

        public override void Shoot(Vector2 origin, Vector2 direction, float distance, LayerMask targets, float damage)
        {
            lineRenderer.SetPosition(0, origin);
            base.Shoot(origin, direction, distance, targets, damage);

            Vector2 secondPoint = hit.collider == null ? direction * distance + origin : hit.point;
            lineRenderer.SetPosition(1, secondPoint);
        }
    }
}