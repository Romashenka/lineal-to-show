﻿using UnityEngine;
using BatProject.UI;
using BatProject.Player;
using System;

namespace BatProject.Items
{
    [CreateAssetMenu(menuName = "Weapons/Weapon")]
    public class Weapon : ScriptableObject
    {
        [SerializeField]
        protected Sprite clipSprite;

        [SerializeField]
        protected int _clipSize = 20;
        public int clipSize { get { return _clipSize; } }

        [SerializeField]
        private Sprite _UISprite;
        public Sprite UISprite { get { return _UISprite; } }

        [SerializeField]
        private WeaponAction firstAction;
        [SerializeField]
        private WeaponAction secondAction;

        [SerializeField, Range(0f, 1f)]
        private float _speedRateIfAiming = .5f;
        public float speedRateIfAiming { get { return _speedRateIfAiming; } }

        [SerializeField]
        private float _shotImpact = 1f;
        public float shotImpact { get { return _shotImpact; } }

        [SerializeField]
        private AudioClip _shotSound;
        public AudioClip shotSound { get { return _shotSound; } }

        [SerializeField]
        private AimStats _aimStats;
        public AimStats aimStats { get { return _aimStats; } }


        public void SetWeapon(PlayerShooter shooter)
        {
            if (firstAction != null)
                firstAction.SetActions(shooter.firstActSet);
            if (secondAction != null)
                secondAction.SetActions(shooter.secondActSet);
        }

        public void SetVisual(EquipmentWeapon equipmentWeapon, IWeaponUI visual)
        {
            visual.mainSprite = _UISprite;
            visual.clipSize = _clipSize;
            visual.clipSprite = clipSprite;
            visual.currentAmmo = equipmentWeapon.ammo;
        }

        public virtual void SetAim(Aim aim)
        {
            if (aim.hidden)
                aim.Hide(false);
            aim.aimStats = aimStats;
        }

        [System.Serializable]
        public struct AimStats
        {
            public float minExtension;
            public float maxExtension;
            [Range(1f, 50f)]
            public float weaponDispersion;
            public float onShotExtension;
            public float onMoveExtension;
        }

    }
}
