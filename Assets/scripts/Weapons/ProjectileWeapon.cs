﻿using UnityEngine;
using BatProject.UI;
using BatProject.Player;

namespace BatProject.Items
{
    // [CreateAssetMenu(menuName = "Weapons/ProjectileWeapon")]
    public class ProjectileWeapon : Weapon
    {
        [SerializeField]
        private Projectile projectilePrefab;

        [SerializeField]
        private float projectileSpeed = 20f;

        [SerializeField]
        private float projectileLifeTime = 1f;

        // public override bool Use(Transform origin, Vector2 direction, EquipmentWeapon equipmentWeapon)
        // {
        //     if (equipmentWeapon.lastShootTime + shootRatePerSecond < Time.time &&
        //          equipmentWeapon.bulletsInClip > 0)
        //     {
        //         equipmentWeapon.bulletsInClip -= shootAmmoCost;

        //         Launch(origin, direction);
        //         return true;
        //     }
        //     else
        //         return false;
        // }

        Projectile Launch(Transform origin, Vector2 direction)
        {
            Projectile projectile = Instantiate(projectilePrefab, origin.position, Quaternion.identity);
            projectile.AddForce(direction * projectileSpeed);
            Destroy(projectile.gameObject, projectileLifeTime);
            return projectile;
        }

        public override void SetAim(Aim aim)
        {
            if (aim.hidden)
                aim.Hide(false);
            aim.aimStats = aimStats;
        }

    }
}