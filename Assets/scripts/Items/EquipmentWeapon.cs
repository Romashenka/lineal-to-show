﻿using System;
using UnityEngine;
using BatProject.UI;
using BatProject.Player;

namespace BatProject.Items
{
    [Serializable]
    public class EquipmentWeapon : EquipmentItem
    {
        private event Action<int> ammoChanged;
        public event Action<int> clipChanged;
        public event Action<Vector2> weaponShot;

        [SerializeField]
        private Weapon _weapon;
        public Weapon weapon { get { return _weapon; } }

        [SerializeField]
        private int _ammo;
        public int ammo
        {
            get
            { return _ammo; }
            set
            {
                _ammo = value;
                if (ammoChanged != null)
                    ammoChanged(value);

            }
        }

        private int _bulletsInClip;
        public int bulletsInClip
        {
            get
            {
                return _bulletsInClip;
            }
            set
            {
                _bulletsInClip = value;
                if (clipChanged != null)
                    clipChanged(value);
            }
        }

        [NonSerialized]
        private float _lastShootTime;
        public float lastShootTime { get { return _lastShootTime; } set { _lastShootTime = value; } }

        private ItemType _itemType = ItemType.Weapon;
        public override ItemType itemType { get { return _itemType; } }

        public EquipmentWeapon(Weapon weapon, int ammo)
        {
            this._weapon = weapon;
            this.ammo = ammo;
        }

        // public void Use(Transform origin, Vector2 direction)
        // {
        //     if (weapon.Use(origin, direction, this))
        //     {
        //         _lastShootTime = Time.time;
        //         if (weaponShot != null)
        //             weaponShot(direction);
        //     }
        // }

        public void OnWeaponShot(Vector2 direction)
        {
            if (weaponShot != null)
                weaponShot(direction);
        }

        public void Reload()
        {
            _ammo += bulletsInClip;
            bulletsInClip = Mathf.Clamp(ammo, 0, weapon.clipSize);
            ammo -= bulletsInClip;
        }

        public void SetVisual(IWeaponUI visual)
        {
            ammoChanged = visual.OnAmmoChanged;
            clipChanged = visual.OnClipChanged;
            weapon.SetVisual(this, visual);
        }
    }
}