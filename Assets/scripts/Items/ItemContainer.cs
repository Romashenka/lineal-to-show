﻿using BatProject.Player;
using UnityEngine;

namespace BatProject.Items
{
    public class ItemContainer : MonoBehaviour, IUsable
    {
        public EquipmentItem item;

        public void OnDetected()
        {
            throw new System.NotImplementedException();
        }

        public void OnLost()
        {
            throw new System.NotImplementedException();
        }

        public void Use(PlayerController user)
        {
            user.equipment.TakeItem(item);
		}
    }
}