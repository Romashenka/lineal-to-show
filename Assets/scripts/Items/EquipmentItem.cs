﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BatProject.Items
{
    public abstract class EquipmentItem
    {
        public abstract ItemType itemType { get; }
    }

    public enum ItemType
    {
        Weapon,

    }
}