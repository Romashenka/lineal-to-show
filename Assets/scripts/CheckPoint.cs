﻿using UnityEngine;
using BatProject.GameSaves;
using BatProject.Player;

namespace BatProject
{
    /// <summary>
    /// Component for gameObject representing check-point.
    /// </summary>
    [RequireComponent(typeof(Collider2D))]
    public class CheckPoint : MonoBehaviour, GameSaves.ISavable, IUsable
    {
        public GameSaves.SavingSystem savingSystem;
        private Collider2D myCollider;

        private IOnOff[] indicators;

        public System.Action checkpointDestroyed;

        private bool used;


        void Awake()
        {
            if (savingSystem == null)
            {
                Debug.Log("No SavingSystem reference", this);
            }
            indicators = GetComponentsInChildren<IOnOff>();
            if (indicators == null)
            {
                Debug.Log("No OnOffIndicatorss for me or my children", this);
            }
            myCollider = GetComponent<Collider2D>();
        }

        public void Use(PlayerController user)
        {
            Activate();
            savingSystem.SaveGame();
        }

        public void OnDetected()
        {
            SwitchIndicators(true);
        }

        public void OnLost()
        {
            if (!used)
                SwitchIndicators(false);
        }

        void SwitchIndicators(bool active)
        {
            foreach (var ind in indicators)
            {
                ind.SetOnOff(active);
            }
        }

        void Activate()
        {
            used = true;
            if (checkpointDestroyed != null)
                checkpointDestroyed();
            Destroy(this);
        }

        public SavedData Save()
        {
            // Destroy method is too slow. GameObject is still !=null on the time of saving;
            if (used)
                return null;
            else
                return new SavedData();
        }

        public bool Load(SavedData saved)
        {
            if (saved == null)
            {
                SwitchIndicators(true);
                Activate();
                Destroy(myCollider); // To not get OnTriggerEnter call after destroy this component.
            }
            return true;
        }

    }
}