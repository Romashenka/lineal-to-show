﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace BatProject.Bible
{
    [CustomEditor(typeof(HolyBible))]
    public class HolyBibleEditor : Editor
    {
        private HolyBible bible;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Export"))
            {

            }
        }

        void CreateJSON()
        {
            string jsoned = JsonUtility.ToJson(bible);

            // void Serialize(System.Object toSetialize)
            // {
            //     using (FileStream fs = new FileStream(, FileMode.OpenOrCreate))
            //     {

            //        formatter.Serialize(fs, toSetialize);
            //     }
            // }
        }

        void OnEnable()
        {
            bible = target as HolyBible;
        }
    }
}